<?
	$fullNav = array(
		__('Dashboard') 	=> $this->Html->url(array('controller' => 'users', 'action' => 'dashboard', 'admin' => true)),
		__('Users') 		=> $this->Html->url(array('controller' => 'users', 'action' => 'index', 'admin' => true)),
		__('CMS') 			=> array(
			'link'				=> array(
				'label' => __('Pages'),
				'url' 	=> $this->Html->url(array('controller' => 'pages', 'action' => 'index', 'admin' => true)),
				'icon' 	=> '<i class="icon-white icon-align-center"></i>',
			),
			__('Films')			=> $this->Html->url(array('controller' => 'films', 'action' => 'index', 'admin' => true)),
			__('Tags') 			=> $this->Html->url(array('controller' => 'tags', 'action' => 'index', 'admin' => true)),
		),
		/* array(
			'link'					=> $this->Html->url(array('controller' => 'users', 'action' => 'index', 'admin' => true)),
			__('Users') 			=> $this->Html->url(array('controller' => 'groups', 'action' => 'view', 'group' => '5148e406-a598-4155-98b9-0f0c46dad844', 'admin' => true)),
		), */
	);
	
	$settingsNav = array(
		__('System Milestones') 		=> $this->Html->url(array('controller' => 'system_milestones', 'action' => 'index', 'manage' => false)),
		__('System Statuses') 			=> $this->Html->url(array('controller' => 'system_statuses', 'action' => 'index', 'admin' => true)),
		__('System Models') 			=> $this->Html->url(array('controller' => 'system_models', 'action' => 'index', 'admin' => true)),
		__('System Events') 			=> $this->Html->url(array('controller' => 'system_events', 'action' => 'index', 'admin' => true)),
		__('System Logs') 				=> $this->Html->url(array('controller' => 'system_logs', 'action' => 'index', 'admin' => true)),
		__('Rebuild ACL') 				=> $this->Html->url(array('controller' => 'Acl', 'action' => 'aco_aro_sync', 'admin' => false)),
	);
?>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<?=$this->Html->link('ITKAfrica', '/', array('class' => 'brand'));?>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<? (empty($bodyClass)) ? $bodyClass = '' : $bodyClass = $bodyClass ; ?>
					<? foreach($fullNav as $key => $link): ?>
						<li <?=($bodyClass == strtolower($key)) ? 'class="dropdown active"' : 'class="dropdown"'; ?>>
							<?/* =(is_array($link) ? 'Y' : 'N');debug($link); */?>
							<? if (is_array($link)): ?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$link['link']['icon'];?> <?=$key;?> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="<?=$link['link']['url'];?>"><i class="icon-chevron-right pull-right"></i> <?=$link['link']['label'];?></a></li>
									<? foreach($link as $subKey => $subLink): ?>
										<? if ($subKey != 'link'): ?>
											<li><a href="<?=$subLink;?>"><i class="icon-chevron-right pull-right"></i> <?=$subKey;?></a></li>
										<? endif; ?>
									<? endforeach; ?>
								</ul>
							<? else: ?>
								<a href="<?=$link;?>"><?=$key;?></a>
							<? endif; ?>
						</li>
					<? endforeach; ?>
					
				</ul>
				<? if(!empty($currentUser)): ?>
					<div class="span2 pull-right">
						<div class="btn-group pull-right">
							<a class="btn btn-primary" href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'logout', 'admin' => false));?>"><i class="icon-off icon-white"></i> <?=__('Logout');?></a>
							<ul class="dropdown-menu">
								<? foreach($settingsNav as $key => $link): ?>
									<li><a href="<?=$link;?>"> <i class="icon-chevron-right pull-right"></i> <?=$key;?></a></li>
								<? endforeach; ?>
							</ul>
							<a href="#" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-wrench icon-white"></i></a>
						</div>
					</div>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>