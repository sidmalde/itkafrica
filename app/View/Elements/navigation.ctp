<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<? if(!empty($currentUser)): ?>
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="brand" href="#">Project name</a>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<li class="active"><a href="#">Dashboard</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">System <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><?=$this->Html->link(__('Groups'), array('controller' => 'groups', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Milestones'), array('controller' => 'milestones', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Tickets'), array('controller' => 'tickets', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Tasks'), array('controller' => 'tasks', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Event Types'), array('controller' => 'event_types', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Events'), array('controller' => 'events', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Logs'), array('controller' => 'logs', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Pages'), array('controller' => 'pages', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Page Histories'), array('controller' => 'page_hoistories', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Statuses'), array('controller' => 'statuses', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Status Types'), array('controller' => 'status_types', 'action' => 'index'));?></li>
								<li><?=$this->Html->link(__('Websites'), array('controller' => 'websites', 'action' => 'index'));?></li>
								
								<li class="divider"></li>
								
								<li class="nav-header">Nav header</li>
								<li><a href="#">Separated link</a></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		<? endif; ?>
	</div>
</div>