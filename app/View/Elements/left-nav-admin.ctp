<?
	$fullNav = array(
		__('Dashboard') 		=> $this->Html->url(array('controller' => 'groups', 'action' => 'index', 'admin' => true)),
		__('Groups')			=> $this->Html->url(array('controller' => 'groups', 'action' => 'index', 'admin' => true)),
		__('Users') 			=> $this->Html->url(array('controller' => 'users', 'action' => 'index', 'admin' => true)),
		__('Milestones') 		=> $this->Html->url(array('controller' => 'milestones', 'action' => 'index', 'admin' => true)),
		__('Tickets') 			=> $this->Html->url(array('controller' => 'tickets', 'action' => 'index', 'admin' => true)),
		__('Tasks') 			=> $this->Html->url(array('controller' => 'tasks', 'action' => 'index', 'admin' => true)),
		__('Event Types') 		=> $this->Html->url(array('controller' => 'event_types', 'action' => 'index', 'admin' => true)),
		__('Events') 			=> $this->Html->url(array('controller' => 'events', 'action' => 'index', 'admin' => true)),
		__('Logs') 				=> $this->Html->url(array('controller' => 'logs', 'action' => 'index', 'admin' => true)),
		__('Pages') 			=> $this->Html->url(array('controller' => 'pages', 'action' => 'index', 'admin' => true)),
		__('Page Histories') 	=> $this->Html->url(array('controller' => 'page_hoistories', 'action' => 'index', 'admin' => true)),
		__('System Statuses') 	=> $this->Html->url(array('controller' => 'system_statuses', 'action' => 'index', 'admin' => true)),
		__('System Models') 	=> $this->Html->url(array('controller' => 'system_models', 'action' => 'index', 'admin' => true)),
		__('System Logs') 		=> $this->Html->url(array('controller' => 'system_logs', 'action' => 'index', 'admin' => true)),
		__('Websites') 			=> $this->Html->url(array('controller' => 'websites', 'action' => 'index', 'admin' => true)),
		__('Rebuild ACL') 		=> $this->Html->url(array('controller' => 'Acl', 'action' => 'aco_aro_sync', 'admin' => false)),
	);
?>
<ul class="nav nav-list affix nav-side-left">
	<? (empty($bodyClass)) ? $bodyClass = '' : $bodyClass = $bodyClass ; ?>
	<? foreach($fullNav as $key => $link): ?>
		<?
			if ($bodyClass == strtolower(str_replace(' ', '-', $key))) {
				$class = 'class="active"';
			} else {
				$class = '';
			}
		?>
		<li <?=$class;?>><a href="<?=$link;?>"><i class="icon-chevron-right pull-right"></i> <?=$key;?></a></li>
	<? endforeach; ?>
</ul>