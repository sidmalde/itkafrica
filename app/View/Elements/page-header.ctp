<?
	/*** Expected Array: ***/
	
	/* $pageHeaderLinks = array(
		0 => array(
			'type' 		=> '',	# 'link' or 'button' or 'submit'
			'label' 	=> '',	# 'label' to display on button
			'url' 		=> '',	# required for type 'link'
			'options'	=> '',	# optional extra arguments (classes, div & label declarations etc...)
		),
	); */
?>

<div class="row">
	<div class="span12">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span4">
				<div class="pull-right">
					<? if (!empty($pageHeaderLinks)): ?>
						<? debug($pageHeaderLinks); ?>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>