<div class="websites view">
<h2><?php  echo __('Website'); ?></h2>
	<dl>
		<dt><?=__('Id'); ?></dt>
		<dd>
			<?=h($website['Website']['id']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Status'); ?></dt>
		<dd>
			<?=$this->Html->link($website['Status']['name'], array('controller' => 'statuses', 'action' => 'view', $website['Status']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?=__('Currency'); ?></dt>
		<dd>
			<?=h($website['Website']['currency']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Display Rrp'); ?></dt>
		<dd>
			<?=h($website['Website']['display_rrp']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Currency Iso'); ?></dt>
		<dd>
			<?=h($website['Website']['currency_iso']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Country Iso2'); ?></dt>
		<dd>
			<?=h($website['Website']['country_iso2']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Country Iso3'); ?></dt>
		<dd>
			<?=h($website['Website']['country_iso3']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Url'); ?></dt>
		<dd>
			<?=h($website['Website']['url']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Language'); ?></dt>
		<dd>
			<?=h($website['Website']['language']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Language Short'); ?></dt>
		<dd>
			<?=h($website['Website']['language_short']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Google Analytics Code'); ?></dt>
		<dd>
			<?=h($website['Website']['google_analytics_code']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Adwords Id'); ?></dt>
		<dd>
			<?=h($website['Website']['adwords_id']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Adwords Language'); ?></dt>
		<dd>
			<?=h($website['Website']['adwords_language']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Adwords Label'); ?></dt>
		<dd>
			<?=h($website['Website']['adwords_label']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Adwords Value'); ?></dt>
		<dd>
			<?=h($website['Website']['adwords_value']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Title'); ?></dt>
		<dd>
			<?=h($website['Website']['title']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Name'); ?></dt>
		<dd>
			<?=h($website['Website']['name']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Description'); ?></dt>
		<dd>
			<?=h($website['Website']['description']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Tagline'); ?></dt>
		<dd>
			<?=h($website['Website']['tagline']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Company'); ?></dt>
		<dd>
			<?=h($website['Website']['company']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Address 1'); ?></dt>
		<dd>
			<?=h($website['Website']['address_1']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Address 2'); ?></dt>
		<dd>
			<?=h($website['Website']['address_2']); ?>
			&nbsp;
		</dd>
		<dt><?=__('City'); ?></dt>
		<dd>
			<?=h($website['Website']['city']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Postcode'); ?></dt>
		<dd>
			<?=h($website['Website']['postcode']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Country'); ?></dt>
		<dd>
			<?=h($website['Website']['country']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Phone'); ?></dt>
		<dd>
			<?=h($website['Website']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Fax'); ?></dt>
		<dd>
			<?=h($website['Website']['fax']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Copyright'); ?></dt>
		<dd>
			<?=h($website['Website']['copyright']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Legal'); ?></dt>
		<dd>
			<?=h($website['Website']['legal']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Support Email'); ?></dt>
		<dd>
			<?=h($website['Website']['support_email']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Sales Email'); ?></dt>
		<dd>
			<?=h($website['Website']['sales_email']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Invoice Email'); ?></dt>
		<dd>
			<?=h($website['Website']['invoice_email']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Keywords'); ?></dt>
		<dd>
			<?=h($website['Website']['keywords']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Created'); ?></dt>
		<dd>
			<?=h($website['Website']['created']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Modified'); ?></dt>
		<dd>
			<?=h($website['Website']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?=__('Actions'); ?></h3>
	<ul>
		<li><?=$this->Html->link(__('Edit Website'), array('action' => 'edit', $website['Website']['id'])); ?> </li>
		<li><?=$this->Form->postLink(__('Delete Website'), array('action' => 'delete', $website['Website']['id']), null, __('Are you sure you want to delete # %s?', $website['Website']['id'])); ?> </li>
		<li><?=$this->Html->link(__('List Websites'), array('action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Website'), array('action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Page Histories'), array('controller' => 'page_histories', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Pages'), array('controller' => 'pages', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?=__('Related Page Histories'); ?></h3>
	<?php if (!empty($website['PageHistory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?=__('Id'); ?></th>
		<th><?=__('Page Id'); ?></th>
		<th><?=__('Parent Page Id'); ?></th>
		<th><?=__('Website Id'); ?></th>
		<th><?=__('Page Layout Id'); ?></th>
		<th><?=__('Dpr'); ?></th>
		<th><?=__('Label'); ?></th>
		<th><?=__('Title'); ?></th>
		<th><?=__('Url'); ?></th>
		<th><?=__('Description'); ?></th>
		<th><?=__('Content'); ?></th>
		<th><?=__('Publish'); ?></th>
		<th><?=__('Moveable'); ?></th>
		<th><?=__('Edit Page Layout'); ?></th>
		<th><?=__('Edit Dpr'); ?></th>
		<th><?=__('Edit Url'); ?></th>
		<th><?=__('Edit Content'); ?></th>
		<th><?=__('Deletable'); ?></th>
		<th><?=__('Children'); ?></th>
		<th><?=__('Position'); ?></th>
		<th><?=__('Expires'); ?></th>
		<th><?=__('Seo Priority'); ?></th>
		<th><?=__('Created'); ?></th>
		<th><?=__('Modified'); ?></th>
		<th class="actions"><?=__('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($website['PageHistory'] as $pageHistory): ?>
		<tr>
			<td><?=$pageHistory['id']; ?></td>
			<td><?=$pageHistory['page_id']; ?></td>
			<td><?=$pageHistory['parent_page_id']; ?></td>
			<td><?=$pageHistory['website_id']; ?></td>
			<td><?=$pageHistory['page_layout_id']; ?></td>
			<td><?=$pageHistory['dpr']; ?></td>
			<td><?=$pageHistory['label']; ?></td>
			<td><?=$pageHistory['title']; ?></td>
			<td><?=$pageHistory['url']; ?></td>
			<td><?=$pageHistory['description']; ?></td>
			<td><?=$pageHistory['content']; ?></td>
			<td><?=$pageHistory['publish']; ?></td>
			<td><?=$pageHistory['moveable']; ?></td>
			<td><?=$pageHistory['edit_page_layout']; ?></td>
			<td><?=$pageHistory['edit_dpr']; ?></td>
			<td><?=$pageHistory['edit_url']; ?></td>
			<td><?=$pageHistory['edit_content']; ?></td>
			<td><?=$pageHistory['deletable']; ?></td>
			<td><?=$pageHistory['children']; ?></td>
			<td><?=$pageHistory['position']; ?></td>
			<td><?=$pageHistory['expires']; ?></td>
			<td><?=$pageHistory['seo_priority']; ?></td>
			<td><?=$pageHistory['created']; ?></td>
			<td><?=$pageHistory['modified']; ?></td>
			<td class="actions">
				<?=$this->Html->link(__('View'), array('controller' => 'page_histories', 'action' => 'view', $pageHistory['id'])); ?>
				<?=$this->Html->link(__('Edit'), array('controller' => 'page_histories', 'action' => 'edit', $pageHistory['id'])); ?>
				<?=$this->Form->postLink(__('Delete'), array('controller' => 'page_histories', 'action' => 'delete', $pageHistory['id']), null, __('Are you sure you want to delete # %s?', $pageHistory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?=__('Related Pages'); ?></h3>
	<?php if (!empty($website['Page'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?=__('Id'); ?></th>
		<th><?=__('Parent Page Id'); ?></th>
		<th><?=__('Website Id'); ?></th>
		<th><?=__('Status Id'); ?></th>
		<th><?=__('Meta Keywords'); ?></th>
		<th><?=__('Meta Description'); ?></th>
		<th><?=__('Dpr'); ?></th>
		<th><?=__('Label'); ?></th>
		<th><?=__('Title'); ?></th>
		<th><?=__('Url'); ?></th>
		<th><?=__('Content'); ?></th>
		<th><?=__('Tags'); ?></th>
		<th><?=__('Children'); ?></th>
		<th><?=__('Position'); ?></th>
		<th><?=__('Seo Priority'); ?></th>
		<th><?=__('Created'); ?></th>
		<th><?=__('Modified'); ?></th>
		<th class="actions"><?=__('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($website['Page'] as $page): ?>
		<tr>
			<td><?=$page['id']; ?></td>
			<td><?=$page['parent_page_id']; ?></td>
			<td><?=$page['website_id']; ?></td>
			<td><?=$page['status_id']; ?></td>
			<td><?=$page['meta_keywords']; ?></td>
			<td><?=$page['meta_description']; ?></td>
			<td><?=$page['dpr']; ?></td>
			<td><?=$page['label']; ?></td>
			<td><?=$page['title']; ?></td>
			<td><?=$page['url']; ?></td>
			<td><?=$page['content']; ?></td>
			<td><?=$page['tags']; ?></td>
			<td><?=$page['children']; ?></td>
			<td><?=$page['position']; ?></td>
			<td><?=$page['seo_priority']; ?></td>
			<td><?=$page['created']; ?></td>
			<td><?=$page['modified']; ?></td>
			<td class="actions">
				<?=$this->Html->link(__('View'), array('controller' => 'pages', 'action' => 'view', $page['id'])); ?>
				<?=$this->Html->link(__('Edit'), array('controller' => 'pages', 'action' => 'edit', $page['id'])); ?>
				<?=$this->Form->postLink(__('Delete'), array('controller' => 'pages', 'action' => 'delete', $page['id']), null, __('Are you sure you want to delete # %s?', $page['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?=$this->Html->link(__('New Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
