<div class="websites form">
<?=$this->Form->create('Website'); ?>
	<fieldset>
		<legend><?=__('Add Website'); ?></legend>
	<?php
		echo $this->Form->input('status_id');
		echo $this->Form->input('currency');
		echo $this->Form->input('display_rrp');
		echo $this->Form->input('currency_iso');
		echo $this->Form->input('country_iso2');
		echo $this->Form->input('country_iso3');
		echo $this->Form->input('url');
		echo $this->Form->input('language');
		echo $this->Form->input('language_short');
		echo $this->Form->input('google_analytics_code');
		echo $this->Form->input('adwords_id');
		echo $this->Form->input('adwords_language');
		echo $this->Form->input('adwords_label');
		echo $this->Form->input('adwords_value');
		echo $this->Form->input('title');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('tagline');
		echo $this->Form->input('company');
		echo $this->Form->input('address_1');
		echo $this->Form->input('address_2');
		echo $this->Form->input('city');
		echo $this->Form->input('postcode');
		echo $this->Form->input('country');
		echo $this->Form->input('phone');
		echo $this->Form->input('fax');
		echo $this->Form->input('copyright');
		echo $this->Form->input('legal');
		echo $this->Form->input('support_email');
		echo $this->Form->input('sales_email');
		echo $this->Form->input('invoice_email');
		echo $this->Form->input('keywords');
	?>
	</fieldset>
<?=$this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?=__('Actions'); ?></h3>
	<ul>

		<li><?=$this->Html->link(__('List Websites'), array('action' => 'index')); ?></li>
		<li><?=$this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Page Histories'), array('controller' => 'page_histories', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Pages'), array('controller' => 'pages', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
	</ul>
</div>
