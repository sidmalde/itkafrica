<div class="websites index">
	<h2><?=__('Websites'); ?></h2>
	<table  class="table table-striped table-bordered table-hover ">
	<tr>
			<th><?=$this->Paginator->sort('id'); ?></th>
			<th><?=$this->Paginator->sort('status_id'); ?></th>
			<th><?=$this->Paginator->sort('currency'); ?></th>
			<th><?=$this->Paginator->sort('display_rrp'); ?></th>
			<th><?=$this->Paginator->sort('currency_iso'); ?></th>
			<th><?=$this->Paginator->sort('country_iso2'); ?></th>
			<th><?=$this->Paginator->sort('country_iso3'); ?></th>
			<th><?=$this->Paginator->sort('url'); ?></th>
			<th><?=$this->Paginator->sort('language'); ?></th>
			<th><?=$this->Paginator->sort('language_short'); ?></th>
			<th><?=$this->Paginator->sort('google_analytics_code'); ?></th>
			<th><?=$this->Paginator->sort('adwords_id'); ?></th>
			<th><?=$this->Paginator->sort('adwords_language'); ?></th>
			<th><?=$this->Paginator->sort('adwords_label'); ?></th>
			<th><?=$this->Paginator->sort('adwords_value'); ?></th>
			<th><?=$this->Paginator->sort('title'); ?></th>
			<th><?=$this->Paginator->sort('name'); ?></th>
			<th><?=$this->Paginator->sort('description'); ?></th>
			<th><?=$this->Paginator->sort('tagline'); ?></th>
			<th><?=$this->Paginator->sort('company'); ?></th>
			<th><?=$this->Paginator->sort('address_1'); ?></th>
			<th><?=$this->Paginator->sort('address_2'); ?></th>
			<th><?=$this->Paginator->sort('city'); ?></th>
			<th><?=$this->Paginator->sort('postcode'); ?></th>
			<th><?=$this->Paginator->sort('country'); ?></th>
			<th><?=$this->Paginator->sort('phone'); ?></th>
			<th><?=$this->Paginator->sort('fax'); ?></th>
			<th><?=$this->Paginator->sort('copyright'); ?></th>
			<th><?=$this->Paginator->sort('legal'); ?></th>
			<th><?=$this->Paginator->sort('support_email'); ?></th>
			<th><?=$this->Paginator->sort('sales_email'); ?></th>
			<th><?=$this->Paginator->sort('invoice_email'); ?></th>
			<th><?=$this->Paginator->sort('keywords'); ?></th>
			<th><?=$this->Paginator->sort('created'); ?></th>
			<th><?=$this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?=__('Actions'); ?></th>
	</tr>
	<?php foreach ($websites as $website): ?>
	<tr>
		<td><?=h($website['Website']['id']); ?>&nbsp;</td>
		<td>
			<?=$this->Html->link($website['Status']['name'], array('controller' => 'statuses', 'action' => 'view', $website['Status']['id'])); ?>
		</td>
		<td><?=h($website['Website']['currency']); ?>&nbsp;</td>
		<td><?=h($website['Website']['display_rrp']); ?>&nbsp;</td>
		<td><?=h($website['Website']['currency_iso']); ?>&nbsp;</td>
		<td><?=h($website['Website']['country_iso2']); ?>&nbsp;</td>
		<td><?=h($website['Website']['country_iso3']); ?>&nbsp;</td>
		<td><?=h($website['Website']['url']); ?>&nbsp;</td>
		<td><?=h($website['Website']['language']); ?>&nbsp;</td>
		<td><?=h($website['Website']['language_short']); ?>&nbsp;</td>
		<td><?=h($website['Website']['google_analytics_code']); ?>&nbsp;</td>
		<td><?=h($website['Website']['adwords_id']); ?>&nbsp;</td>
		<td><?=h($website['Website']['adwords_language']); ?>&nbsp;</td>
		<td><?=h($website['Website']['adwords_label']); ?>&nbsp;</td>
		<td><?=h($website['Website']['adwords_value']); ?>&nbsp;</td>
		<td><?=h($website['Website']['title']); ?>&nbsp;</td>
		<td><?=h($website['Website']['name']); ?>&nbsp;</td>
		<td><?=h($website['Website']['description']); ?>&nbsp;</td>
		<td><?=h($website['Website']['tagline']); ?>&nbsp;</td>
		<td><?=h($website['Website']['company']); ?>&nbsp;</td>
		<td><?=h($website['Website']['address_1']); ?>&nbsp;</td>
		<td><?=h($website['Website']['address_2']); ?>&nbsp;</td>
		<td><?=h($website['Website']['city']); ?>&nbsp;</td>
		<td><?=h($website['Website']['postcode']); ?>&nbsp;</td>
		<td><?=h($website['Website']['country']); ?>&nbsp;</td>
		<td><?=h($website['Website']['phone']); ?>&nbsp;</td>
		<td><?=h($website['Website']['fax']); ?>&nbsp;</td>
		<td><?=h($website['Website']['copyright']); ?>&nbsp;</td>
		<td><?=h($website['Website']['legal']); ?>&nbsp;</td>
		<td><?=h($website['Website']['support_email']); ?>&nbsp;</td>
		<td><?=h($website['Website']['sales_email']); ?>&nbsp;</td>
		<td><?=h($website['Website']['invoice_email']); ?>&nbsp;</td>
		<td><?=h($website['Website']['keywords']); ?>&nbsp;</td>
		<td><?=h($website['Website']['created']); ?>&nbsp;</td>
		<td><?=h($website['Website']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?=$this->Html->link(__('View'), array('action' => 'view', $website['Website']['id'])); ?>
			<?=$this->Html->link(__('Edit'), array('action' => 'edit', $website['Website']['id'])); ?>
			<?=$this->Form->postLink(__('Delete'), array('action' => 'delete', $website['Website']['id']), null, __('Are you sure you want to delete # %s?', $website['Website']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?=__('Actions'); ?></h3>
	<ul>
		<li><?=$this->Html->link(__('New Website'), array('action' => 'add')); ?></li>
		<li><?=$this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Page Histories'), array('controller' => 'page_histories', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Pages'), array('controller' => 'pages', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
	</ul>
</div>
