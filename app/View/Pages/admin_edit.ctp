<div class="pages form">
<?=$this->Form->create('Page'); ?>
	<fieldset>
		<legend><?=__('Edit Page'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('parent_page_id');
		echo $this->Form->input('website_id');
		echo $this->Form->input('status_id');
		echo $this->Form->input('meta_keywords');
		echo $this->Form->input('meta_description');
		echo $this->Form->input('dpr');
		echo $this->Form->input('label');
		echo $this->Form->input('title');
		echo $this->Form->input('url');
		echo $this->Form->input('content');
		echo $this->Form->input('tags');
		echo $this->Form->input('children');
		echo $this->Form->input('position');
		echo $this->Form->input('seo_priority');
	?>
	</fieldset>
<?=$this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?=__('Actions'); ?></h3>
	<ul>

		<li><?=$this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Page.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Page.id'))); ?></li>
		<li><?=$this->Html->link(__('List Pages'), array('action' => 'index')); ?></li>
		<li><?=$this->Html->link(__('List Pages'), array('controller' => 'pages', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Parent Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Websites'), array('controller' => 'websites', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Website'), array('controller' => 'websites', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Page Histories'), array('controller' => 'page_histories', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
	</ul>
</div>
