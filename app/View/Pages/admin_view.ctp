<div class="pages view">
<h2><?php  echo __('Page'); ?></h2>
	<dl>
		<dt><?=__('Id'); ?></dt>
		<dd>
			<?=h($page['Page']['id']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Parent Page'); ?></dt>
		<dd>
			<?=$this->Html->link($page['ParentPage']['title'], array('controller' => 'pages', 'action' => 'view', $page['ParentPage']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?=__('Website'); ?></dt>
		<dd>
			<?=$this->Html->link($page['Website']['title'], array('controller' => 'websites', 'action' => 'view', $page['Website']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?=__('Status'); ?></dt>
		<dd>
			<?=$this->Html->link($page['Status']['name'], array('controller' => 'statuses', 'action' => 'view', $page['Status']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?=__('Meta Keywords'); ?></dt>
		<dd>
			<?=h($page['Page']['meta_keywords']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Meta Description'); ?></dt>
		<dd>
			<?=h($page['Page']['meta_description']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Dpr'); ?></dt>
		<dd>
			<?=h($page['Page']['dpr']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Label'); ?></dt>
		<dd>
			<?=h($page['Page']['label']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Title'); ?></dt>
		<dd>
			<?=h($page['Page']['title']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Url'); ?></dt>
		<dd>
			<?=h($page['Page']['url']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Content'); ?></dt>
		<dd>
			<?=h($page['Page']['content']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Tags'); ?></dt>
		<dd>
			<?=h($page['Page']['tags']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Children'); ?></dt>
		<dd>
			<?=h($page['Page']['children']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Position'); ?></dt>
		<dd>
			<?=h($page['Page']['position']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Seo Priority'); ?></dt>
		<dd>
			<?=h($page['Page']['seo_priority']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Created'); ?></dt>
		<dd>
			<?=h($page['Page']['created']); ?>
			&nbsp;
		</dd>
		<dt><?=__('Modified'); ?></dt>
		<dd>
			<?=h($page['Page']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?=__('Actions'); ?></h3>
	<ul>
		<li><?=$this->Html->link(__('Edit Page'), array('action' => 'edit', $page['Page']['id'])); ?> </li>
		<li><?=$this->Form->postLink(__('Delete Page'), array('action' => 'delete', $page['Page']['id']), null, __('Are you sure you want to delete # %s?', $page['Page']['id'])); ?> </li>
		<li><?=$this->Html->link(__('List Pages'), array('action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page'), array('action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Pages'), array('controller' => 'pages', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Parent Page'), array('controller' => 'pages', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Websites'), array('controller' => 'websites', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Website'), array('controller' => 'websites', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
		<li><?=$this->Html->link(__('List Page Histories'), array('controller' => 'page_histories', 'action' => 'index')); ?> </li>
		<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?=__('Related Page Histories'); ?></h3>
	<?php if (!empty($page['PageHistory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?=__('Id'); ?></th>
		<th><?=__('Page Id'); ?></th>
		<th><?=__('Parent Page Id'); ?></th>
		<th><?=__('Website Id'); ?></th>
		<th><?=__('Page Layout Id'); ?></th>
		<th><?=__('Dpr'); ?></th>
		<th><?=__('Label'); ?></th>
		<th><?=__('Title'); ?></th>
		<th><?=__('Url'); ?></th>
		<th><?=__('Description'); ?></th>
		<th><?=__('Content'); ?></th>
		<th><?=__('Publish'); ?></th>
		<th><?=__('Moveable'); ?></th>
		<th><?=__('Edit Page Layout'); ?></th>
		<th><?=__('Edit Dpr'); ?></th>
		<th><?=__('Edit Url'); ?></th>
		<th><?=__('Edit Content'); ?></th>
		<th><?=__('Deletable'); ?></th>
		<th><?=__('Children'); ?></th>
		<th><?=__('Position'); ?></th>
		<th><?=__('Expires'); ?></th>
		<th><?=__('Seo Priority'); ?></th>
		<th><?=__('Created'); ?></th>
		<th><?=__('Modified'); ?></th>
		<th class="actions"><?=__('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($page['PageHistory'] as $pageHistory): ?>
		<tr>
			<td><?=$pageHistory['id']; ?></td>
			<td><?=$pageHistory['page_id']; ?></td>
			<td><?=$pageHistory['parent_page_id']; ?></td>
			<td><?=$pageHistory['website_id']; ?></td>
			<td><?=$pageHistory['page_layout_id']; ?></td>
			<td><?=$pageHistory['dpr']; ?></td>
			<td><?=$pageHistory['label']; ?></td>
			<td><?=$pageHistory['title']; ?></td>
			<td><?=$pageHistory['url']; ?></td>
			<td><?=$pageHistory['description']; ?></td>
			<td><?=$pageHistory['content']; ?></td>
			<td><?=$pageHistory['publish']; ?></td>
			<td><?=$pageHistory['moveable']; ?></td>
			<td><?=$pageHistory['edit_page_layout']; ?></td>
			<td><?=$pageHistory['edit_dpr']; ?></td>
			<td><?=$pageHistory['edit_url']; ?></td>
			<td><?=$pageHistory['edit_content']; ?></td>
			<td><?=$pageHistory['deletable']; ?></td>
			<td><?=$pageHistory['children']; ?></td>
			<td><?=$pageHistory['position']; ?></td>
			<td><?=$pageHistory['expires']; ?></td>
			<td><?=$pageHistory['seo_priority']; ?></td>
			<td><?=$pageHistory['created']; ?></td>
			<td><?=$pageHistory['modified']; ?></td>
			<td class="actions">
				<?=$this->Html->link(__('View'), array('controller' => 'page_histories', 'action' => 'view', $pageHistory['id'])); ?>
				<?=$this->Html->link(__('Edit'), array('controller' => 'page_histories', 'action' => 'edit', $pageHistory['id'])); ?>
				<?=$this->Form->postLink(__('Delete'), array('controller' => 'page_histories', 'action' => 'delete', $pageHistory['id']), null, __('Are you sure you want to delete # %s?', $pageHistory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?=$this->Html->link(__('New Page History'), array('controller' => 'page_histories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
