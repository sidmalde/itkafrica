<div class="row">
	<div class="span12 page-header">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span3">
				<div class="pull-right">
					<?=$this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-primary'));?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span12 page-header">
		<h2><?=__('Pages'); ?></h2>
		<table  class="table table-striped table-bordered table-hover ">
		<tr>
				<th><?=$this->Paginator->sort('system_status_id'); ?></th>
				<th><?=$this->Paginator->sort('dpr'); ?></th>
				<th><?=$this->Paginator->sort('label'); ?></th>
				<th><?=$this->Paginator->sort('title'); ?></th>
				<th><?=$this->Paginator->sort('url'); ?></th>
				<th><?=$this->Paginator->sort('modified'); ?></th>
				<th class="actions"><?=__('Actions'); ?></th>
		</tr>
		<?php foreach ($pages as $page): ?>
		<tr>
			<td><?=$page['SystemStatus']['name'];?></td>
			<td><?=h($page['Page']['dpr']); ?>&nbsp;</td>
			<td><?=h($page['Page']['label']); ?>&nbsp;</td>
			<td><?=h($page['Page']['title']); ?>&nbsp;</td>
			<td><?=h($page['Page']['url']); ?>&nbsp;</td>
			<td><?=$this->Time->niceDate($page['Page']['modified']); ?>&nbsp;</td>
			<td>
				<div class="btn-group">
					<?=$this->Html->link(__('View'), array('action' => 'view', 'page' => $page['Page']['id'])); ?>
					<?=$this->Html->link(__('Edit'), array('action' => 'edit', 'page' => $page['Page']['id'])); ?>
					<?=$this->Form->link(__('Delete'), array('action' => 'delete', 'page' => $page['Page']['id']), null, __('Are you sure you want to delete %s?', $page['Page']['title'])); ?>
				</div>
			</td>
		</tr>
		<? endforeach; ?>
		</table>
		<p>
		<?
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
	</div>
</div>