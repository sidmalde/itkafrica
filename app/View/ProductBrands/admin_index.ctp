<? if (!empty($productBrands)): ?>
	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th><?=__('Name');?></th>
			<th><?=__('Status');?></th>
			<th><?=__('Description');?></th>
			<th><?=__('Display Order');?></th>
			<th><?=__('Last Modified');?></th>
			<th>&nbsp;</th>
		</tr>
		<? foreach($productBrands as $productBrand): ?>
			<tr>
				<td><?=$productBrand['ProductBrand']['name'];?></td>
				<td><?=$productBrand['SystemStatus']['name'];?></td>
				<td><?=$productBrand['ProductBrand']['description'];?></td>
				<td><?=$productBrand['ProductBrand']['display_order'];?></td>
				<td><?=$this->Time->niceDate($productBrand['ProductBrand']['modified']);?></td>
				<td>
					<div class="btn-group pull-right">
						<?=$this->Html->link(__('Edit'), array('action' => 'view', 'productBrand' => $productBrand['ProductBrand']['id']), array('class' => 'btn btn-info'));?>
						<?=$this->Html->link(__('Delete'), array('action' => 'delete', 'productBrand' => $productBrand['ProductBrand']['id']), array('class' => 'btn btn-danger'), __('Are you sure?'));?>
					</div>
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<? else: ?>
	<div class="alert alert-info">
		<p class="text-info text-center lead"><strong><?=__('There are currently no Brands.');?></strong></p>
		<p class="text-center"><?=$this->Html->link('<i class="icon-white icon-plus-sign"></i> '.__('New Brand'), array('action' => 'add', 'admin' => true), array('escape' => false, 'class' => 'btn btn-primary'));?></p>
	</div>
<? endif; ?>