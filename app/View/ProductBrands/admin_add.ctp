<div class="well">
	<?=$this->Form->create('ProductBrand', array('url' => array('action' => 'add')));?>
		<?=$this->Form->input('system_status_id', array('class' => 'span6', 'empty' => __('Please select a Status: ')));?>
		<?=$this->Form->input('name', array('class' => 'span6'));?>
		<?=$this->Form->input('description', array('type' => 'textarea', 'class' => 'span6'));?>
		<?=$this->Form->input('display_order', array('class' => 'span6'));?>
		<?=$this->Form->button('Save', array('type' => 'submit', 'class' => 'btn btn-success'));?>
	<?=$this->Form->end();?>
</div>