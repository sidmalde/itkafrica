<div class="well">
	<?=$this->Form->create('ProductCategory', array('url' => Router::url(array('action' => 'edit', 'productCategory' => $productCategory['ProductCategory']['id']))));?>
		<?=$this->Form->input('id');?>
		<?=$this->Form->input('parent_product_category_id', array('class' => 'span6', 'empty' => __('Please select a category: '), 'selected' => $productCategory['ProductCategory']['parent_product_category_id']));?>
		<?=$this->Form->input('name', array('class' => 'span6'));?>
		<?=$this->Form->input('summary', array('type' => 'textarea', 'class' => 'span6'));?>
		<?=$this->Form->input('description', array('type' => 'textarea', 'class' => 'span6'));?>
		<?=$this->Form->button('Save', array('type' => 'submit', 'class' => 'btn btn-success'));?>
	<?=$this->Form->end();?>
</div>