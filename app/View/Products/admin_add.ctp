<div class="well">
	<?=$this->Form->create('Product', array('url' => array('action' => 'add')));?>
		<?=$this->Form->input('product_category_id', array('class' => 'span6', 'empty' => __('Please select a Category: ')));?>
		<?=$this->Form->input('product_brand_id', array('class' => 'span6', 'empty' => __('Please select a Brand: ')));?>
		<?=$this->Form->input('system_status_id', array('class' => 'span6', 'empty' => __('Please select a Status: ')));?>
		<?=$this->Form->input('code', array('class' => 'span6'));?>
		<?=$this->Form->input('supplier_code', array('class' => 'span6'));?>
		<?=$this->Form->input('name', array('class' => 'span6'));?>
		<?=$this->Form->input('description', array('type' => 'textarea', 'class' => 'span6'));?>
		<?=$this->Form->input('stock_level', array('class' => 'span6'));?>
		<?=$this->Form->input('price', array('class' => 'span6'));?>
		<?=$this->Form->input('display_order', array('class' => 'span6'));?>
		<?=$this->Form->input('display_brand', array('class' => 'span6'));?>
		<?=$this->Form->button('Save', array('type' => 'submit', 'class' => 'btn btn-success'));?>
	<?=$this->Form->end();?>
</div>