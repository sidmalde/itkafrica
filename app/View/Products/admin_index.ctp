<? if (!empty($products)): ?>
	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th><?=__('Brand');?></th>
			<th><?=__('Name');?></th>
			<th><?=__('Category');?></th>
			<th><?=__('Status');?></th>
			<th><?=__('Parent Category');?></th>
			<th><?=__('Last Modified');?></th>
			<th>&nbsp;</th>
		</tr>
		<? foreach($products as $product): ?>
			<tr>
				<td><?=$product['ProductBrand']['name'];?></td>
				<td><?=$product['Product']['name'];?></td>
				<td><?=$product['ProductCategory']['name'];?></td>
				<td><?=$product['SystemStatus']['name'];?></td>
				<td><?=$this->Time->niceDate($product['Product']['modified']);?></td>
				<td>
					<div class="btn-group pull-right">
						<?=$this->Html->link(__('Edit'), array('action' => 'view', 'product' => $product['Product']['id']), array('class' => 'btn btn-info'));?>
						<?=$this->Html->link(__('Delete'), array('action' => 'delete', 'product' => $product['Product']['id']), array('class' => 'btn btn-danger'), __('Are you sure?'));?>
					</div>
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<? else: ?>
	<div class="alert alert-info">
		<p class="text-info text-center lead"><strong><?=__('There are currently no Products.');?></strong></p>
		<p class="text-center"><?=$this->Html->link('<i class="icon-white icon-plus-sign"></i> '.__('New Product'), array('action' => 'add', 'admin' => true), array('escape' => false, 'class' => 'btn btn-primary'));?></p>
	</div>
<? endif; ?>