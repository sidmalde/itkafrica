<div class="row">
	<div class="span12 page-header">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span3">
				<div class="pull-right">
					<?=$this->Html->link(__('New Group'), array('action' => 'add'), array('class' => 'btn btn-primary'));?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="car-view-tabbing">
				<? if($currentUser['Group']['id'] == '51488314-33c4-4394-8d02-0f0c46dad844'): //admin only ?>
					<li class="active"><a href="#carInfo" data-toggle="tab"><?=__('Info');?> <i class="icon icon-edit"></i></a></li>
					<li><a href="#carAddress" data-toggle="tab"><?=__('Address');?> <i class="icon icon-edit"></i></a></li>
				<? endif; ?>
			</ul>
			<div class="well">
				<?=$this->Form->create('Group'); ?>
					<?=$this->Form->input('name');?>
					<?=$this->Form->input('description');?>
				<?=$this->Form->end(__('Submit')); ?>
			</div>
		</div>
	</div>
</div>