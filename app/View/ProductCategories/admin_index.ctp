<? if (!empty($productCategories)): ?>
	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th><?=__('Name');?></th>
			<th><?=__('Summary');?></th>
			<th><?=__('Parent Category');?></th>
			<th><?=__('Last Modified');?></th>
			<th>&nbsp;</th>
		</tr>
		<? foreach($productCategories as $productCategory): ?>
			<tr>
				<td><?=$productCategory['ProductCategory']['name'];?></td>
				<td><?=$productCategory['ProductCategory']['summary'];?></td>
				<td><?=$productCategory['ParentProductCategory']['name'];?></td>
				<td><?=$this->Time->niceDate($productCategory['ProductCategory']['modified']);?></td>
				<td>
					<div class="btn-group pull-right">
						<?=$this->Html->link(__('Edit'), array('action' => 'view', 'productCategory' => $productCategory['ProductCategory']['id']), array('class' => 'btn btn-info'));?>
						<?=$this->Html->link(__('Delete'), array('action' => 'delete', 'productCategory' => $productCategory['ProductCategory']['id']), array('class' => 'btn btn-danger'), __('Are you sure?'));?>
					</div>
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<? else: ?>
	<div class="alert alert-info">
		<p class="text-info text-center lead"><strong><?=__('There are currently no Product Categories.');?></strong></p>
		<p class="text-center"><?=$this->Html->link(__('Add New Product Category'), array('action' => 'add', 'admin' => true), array('class' => 'btn btn-primary'));?></p>
	</div>
<? endif; ?>