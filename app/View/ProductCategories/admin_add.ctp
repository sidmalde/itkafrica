<div class="well">
	<?=$this->Form->create('ProductCategory', array('url' => array('action' => 'add')));?>
		<?=$this->Form->input('parent_product_category_id', array('class' => 'span6', 'empty' => __('Please select a Parent Category: ')));?>
		<?=$this->Form->input('name', array('class' => 'span6'));?>
		<?=$this->Form->input('summary', array('type' => 'textarea', 'class' => 'span6'));?>
		<?=$this->Form->input('description', array('type' => 'textarea', 'class' => 'span6'));?>
		<?=$this->Form->button('Save', array('type' => 'submit', 'class' => 'btn btn-success'));?>
	<?=$this->Form->end();?>
</div>