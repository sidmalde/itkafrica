<div class="row">
	<div class="span12 page-header">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span3">
				<div class="pull-right">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span12">
		<ul class="nav nav-tabs" id="myTab">
			<?
				$tabLinks = array(
					0 => array(
						'href' => '#logs',
						'text' => __('Logs'),
						'class' => ' class="active"',
					),
					1 => array(
						'href' => '#myProfile',
						'text' => __('My Profile'),
						'class' => '',
					),
				);
				if (!empty($selectedTab)) {
					foreach ($tabLinks as $index => $tabLink) {
						if ($tabLink['href'] == '#'.$selectedTab) {
							$tabLinks[$index]['class'] = ' class="active"';
						} else {
							$tabLinks[$index]['class'] = '';
						}
					}
				}
			?>
			<? foreach ($tabLinks as $tabLink): ?>
				<li<?=$tabLink['class'];?>><a href="<?=$tabLink['href'];?>" data-toggle="tab"><?=$tabLink['text'];?></a></li>
			<? endforeach; ?>
		</ul>
		
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane <?=(!empty($selectedTab) && $selectedTab == 'logs') ? 'active': '';?>" id="logs">
				<div class="row">
					<div class="span12">
						<? if (!empty($systemLogs)): ?>
							<table class="table table-bordered table-hover table-striped table-condensed">
								<tr>
									<th><?=__('Event');?></th>
									<th><?=__('Description');?></th>
									<th class="hidden-phone"><?=__('User Ref');?></th>
									<th class="hidden-phone"><?=__('User Email');?></th>
									<th>&nbsp;</th>
								</tr>
								<? foreach ($systemLogs as $systemLog): ?>
									<?
										if ($systemLog['Event']['status_id'] == 7) {
											$rowClass = 'class="info"';
										} elseif ($systemLog['Event']['status_id'] == 8) {
											$rowClass = 'class="warning"';
										} elseif ($systemLog['Event']['status_id'] == 9) {
											$rowClass = 'class="error"';
										} else {
											$rowClass = 'class="success"';
										}
									?>
									<tr <?=$rowClass;?>>
										<td><?=$systemLog['Event']['name'];?></td>
										<td><?=$systemLog['Log']['description'];?></td>
										<td class="hidden-phone"><?=$systemLog['User']['fullname'];?></td>
										<td class="hidden-phone"><?=$systemLog['User']['email'];?></td>
										<td>
												<?=$this->Html->link(__('Go'), $systemLog['Log']['url'], array('class' => 'btn btn-success'));?>
										</td>
									</tr>
								<? endforeach; ?>
							</table>
						<? endif; ?>
					</div>
				</div>
			</div>
			<div class="tab-pane <?=(!empty($selectedTab) && $selectedTab == 'myProfile') ? 'active': '';?>" id="myProfile">
				<?=$this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'edit', 'user' => $currentUser['id'])));?>
					<div class="row">
						<div class="span6">
							<div class="well">
								<?=$this->Form->hidden('id');?>
								<?=$this->Form->input('email');?>
								<?=$this->Form->input('mobile');?>
								<?=$this->Form->input('phone');?>
							</div>
						</div>
						<div class="span6">
							<div class="well">
								<?=$this->Form->input('password', array('class' => 'span4', 'value' => false, 'type' => 'password'));?>
								<?=$this->Form->input('new_password', array('class' => 'span4', 'value' => false, 'type' => 'password'));?>
								<?=$this->Form->input('confirm_password', array('class' => 'span4', 'value' => false, 'type' => 'password'));?>
							</div>
						</div>
					</div>
					<div class="row">
						<span class="span12">
							<?=$this->Form->button(__('Save Changes'), array('type' => 'submit', 'class' => 'btn btn-primary'));?>
						</span>
					</div>
				<?=$this->Form->end();?>
			</div>
		</div>
	</div>
</div>
