<?=$this->Form->create('User'); ?>
<div class="row">
	<div class="span12 page-header">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span3">
				<div class="btn-group pull-right">
					<?=$this->Form->button(__('Save'), array('type' => 'submit', 'class' => 'btn btn-primary'));?>
					<?=$this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-danger'));?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<div class="well">
			<div class="row">
				<div class="span6">
					<?=$this->Form->input('group_id', array('empty' => __('Please select a Group'), 'required' => 'required'));?>
					<?=$this->Form->input('title', array('empty' => __('Please select a Title'), 'options' => $userTitles));?>
					<?=$this->Form->input('firstname', array('required' => 'required'));?>
					<?=$this->Form->input('lastname', array('required' => 'required'));?>
				</div>
				<div class="span5">
					<?=$this->Form->input('email', array('type' => 'email', 'required' => 'required'));?>
					<?=$this->Form->input('password', array('required' => 'required'));?>
					<div class="input-append date input-datepicker">
						<?=$this->Form->input('date_of_birth', array('type' => 'text', 'div' => false, 'data-format' => 'yyyy-MM-dd'));?>
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>
						</span>
					</div>
					<?=$this->Form->input('mobile');?>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->Form->end(); ?>