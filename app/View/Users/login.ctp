<?=$this->Form->create('User', array('url' => array('action' => 'login'), 'class' => 'form-login'));?>	
<?=$this->Form->inputs(array(
	'legend' => __('Login'),
	'email',
	'password'
));?>
<?=$this->Form->button(__('Login'), array('type' => 'submit', 'class' => 'btn btn-primary'));?>
<?=$this->Form->end();?>