<div class="row">
	<div class="span12 page-header">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span3">
				<div class="pull-right">
					<?=$this->Html->link(__('New Group'), array('action' => 'add'), array('class' => 'btn btn-primary'));?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span12">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#home" data-toggle="tab"><?=__('Home');?></a></li>
			<li><a href="#myProfile" data-toggle="tab"><?=__('My Profile');?></a></li>
		</ul>
		
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane active in" id="home">
				<div class="span1">
					<a href="<?=Router::url(array('controller' => 'users', 'action' => 'dashboard', 'manage' => true));?>">
						<div class="box-3-3 center">
							<span class="thumb"><?=$this->Html->image('/img/thumbs/admin.png', array('title' => __('Admin Section'), 'alt' => __('Admin Section')));?></span>
							<span><?=__('Admin Section');?></span>
						</div>
					</a>
				</div>
				<div class="span1">
					<a href="<?=Router::url(array('controller' => 'users', 'action' => 'dashboard', 'manage' => true));?>">
						<div class="box-3-3 center">
							<span class="thumb"><?=$this->Html->image('/img/thumbs/users.png', array('title' => __('Manage Users'), 'alt' => __('Manage Users')));?></span>
							<span><?=__('Manage Drivers');?></span>
						</div>
					</a>
				</div>
				<div class="span1">
					<a href="<?=Router::url(array('controller' => 'milestones', 'action' => 'index', 'manage' => false));?>">
						<div class="box-3-3 center">
							<span class="thumb"><?=$this->Html->image('/img/thumbs/ticket.png', array('title' => __('Tickets Section'), 'alt' => __('Tickets Section')));?></span>
							<span><?=__('Manage Tickets');?></span>
						</div>
					</a>
				</div>
			</div>
			<div class="tab-pane" id="myProfile">
				
			</div>
		</div>
	</div>
</div>
