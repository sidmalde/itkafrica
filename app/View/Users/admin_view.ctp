<div class="row">
	<div class="span12 page-header">
		<div class="row">
			<div class="span8"><h3><?=@$pageTitle;?></h3></div>
			<div class="span3">
				<div class="pull-right">
					<?=$this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-primary'));?>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->Form->create('User', array('url' => $this->Html->url(array('controller' => 'users', 'action' => 'edit', 'user' => $user['User']['id'])), 'class' => 'form', 'type' => 'file')); ?>
	<div class="row">
		<div class="span12 page-header">
			<div class="row">
				<div class="span8"></div>
				<div class="span3">
					<div class="btn-group pull-right">
						<?=$this->Form->submit(__('Save Changes'), array('div' => false, 'label' => false, 'type' => 'submit', 'class' => 'btn btn-primary'));?>
						<?=$this->Html->link(__('New User'), array('action' => 'add'), array('class' => 'btn btn-success'));?>
						<? if ($user['Group']['id'] == '5148e406-a598-4155-98b9-0f0c46dad844'): ?>
							<?=$this->Html->link(__('Back to Drivers'), array('controller' => 'groups', 'action' => 'view', 'group' => '5148e406-a598-4155-98b9-0f0c46dad844', 'admin' => true), array('class' => 'btn btn-danger'));?>
						<? else: ?>
							<?=$this->Html->link(__('Back to Clients'), array('controller' => 'groups', 'action' => 'view', 'group' => '5148e587-2720-4203-a3d0-0f0c46dad844', 'admin' => true), array('class' => 'btn btn-danger'));?>
						<? endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<div class="tabbable">
				<ul class="nav nav-tabs" id="user-view-tabbing">
					<li class="active"><a href="#basicInformation"><?=__('Info');?></a></li>
					<li><a href="#extraInformation"><?=__('Extra Info.');?></a></li>
					<!--<li><a href="#settings"><?=__('Additional Settings');?></a></li>-->
				</ul>
		 
				<div id="user-view-content" class="tab-content">
					<div class="tab-pane active in" id="basicInformation">
						<div class="well">
							<div class="row">
								<div class="span6">
									<?=$this->Form->hidden('id');?>
									<?=$this->Form->input('group_id');?>
									<?/* =$this->Form->input('status_id'); */?>
									<?=$this->Form->input('email');?>
									<?/* =$this->Form->input('password'); */?>
									<?=$this->Form->input('title', array('empty' => __('Please select a country'), 'options' => $userTitles));?>
									<?=$this->Form->input('firstname');?>
									<?=$this->Form->input('lastname');?>
									<?/* =$this->Form->input('gender'); */?>
									<div class="input-append date input-datepicker required">
										<?=$this->Form->input('date_of_birth', array('type' => 'text', 'div' => false, 'data-format' => 'yyyy-MM-dd'));?>
										<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
									</div>
									<?=$this->Form->input('phone');?>
								</div>
								<div class="span5">
									<?=$this->Form->input('address_1');?>
									<?=$this->Form->input('address_2');?>
									<?=$this->Form->input('address_3');?>
									<?=$this->Form->input('city');?>
									<?=$this->Form->input('region');?>
									<?=$this->Form->input('postcode');?>
									<?=$this->Form->input('country', array('empty' => __('Please select a country'), 'options' => $countries));?>
									<?=$this->Form->input('mobile');?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="extraInformation">
						<div class="well">
							<div class="row">
								<div class="span6">
									<h3><?=__('Attachments');?></h3>
									<?$collapseOffset=0;?>
									<? if (!empty($user['UserAttachment'])): ?>
										<div class="accordion" id="user-attachments">
											<? foreach ($user['UserAttachment'] as $index => $userAttachment): ?>
												<div class="accordion-group success">
													<div class="accordion-heading">
														<a data-toggle="collapse" data-parent="#user-attachments" href="#collapse<?=$index;?>" class="alignLeft accordion-toggle btn btn-warning">
															<?=$userAttachment['label'];?>
														</a>
													</div>
													<div id="collapse<?=$index;?>" class="accordion-body collapse">
														<div class="accordion-inner">
															<p class="alert alert-info">	
																<strong><?=__('Uploaded By: ', true);?></strong><?=@$users[$userAttachment['user_id']];?><br />
																<strong><?=__('Size: ', true);?></strong><?=round($userAttachment['filesize']/1024).'Kb';?><br />
																<?=nl2br(Sanitize::html($userAttachment['description']));?>
															</p>
															<div class="btn-group">
																<?=$this->Html->link(__('Download'), $userAttachment['location'], array('target' => '_blank', 'class' => 'btn btn-primary'));?>
																<?=$this->Html->link(__('Delete'), array('action' => 'delete_attachment', 'userAttachment' => $userAttachment['id']), array('class' => 'btn btn-danger'));?>
															</div>
														</div>
													</div>
												</div>
												<?$collapseOffset = $index+1;?>
											<? endforeach; ?>
										</div>
									<? else: ?>
										<div class="alert"><?=__('No Attachments have been added yet');?></div>
									<? endif; ?>
									<!--<input id="UserAttachment" type="file" class="hide input-file-real" data-real-input="UserAttachment" data-fake-input="UserFalseAttachment">-->
									<?=$this->Form->input('attachment', array('type' => 'file', 'class' => 'input-file-real', 'style' => 'display: none;', 'data-real-input' => 'UserAttachment', 'data-fake-input' => 'UserFalseAttachment', 'label' => false, 'div' => false));?>
									<div class="input-append file input">
										<?=$this->Form->input('false_attachment', array('class' => 'span4', 'label' => false, 'div' => false, 'disabled' => 'disabled'));?>
										<?=$this->Html->link(__('Browse'), '#', array('class' => 'btn btn-primary', 'escape' => false, 'onclick' => "$('input.input-file-real').click();"));?>
										<?=$this->Html->link(__('Clear'), '#', array('data-real-input' => 'UserAttachment', 'data-fake-input' => 'UserFalseAttachment', 'class' => 'input-file-real-clear btn btn-danger', 'escape' => false));?>
									</div>
								</div>
								<div class="span5 pull-left">
									<h3><?=__('Notes');?></h3>
									<? if (!empty($user['UserNote'])): ?>
										<div class="accordion" id="user-notes">
											<? foreach ($user['UserNote'] as $index => $userNote): ?>
												<?$collapseIndex=$index+$collapseOffset;?>
												<div class="accordion-group success">
													<div class="accordion-heading">
														<a data-toggle="collapse" data-parent="#user-notes" href="#collapse<?=$collapseIndex;?>" class="alignLeft accordion-toggle btn btn-warning">
															<?=$userNote['summary'];?>
														</a>
													</div>
													<div id="collapse<?=$collapseIndex;?>" class="accordion-body collapse">
														<div class="accordion-inner">
															<p class="alert alert-info"><strong><?=__('Created By: ', true);?></strong><?=@$users[$userNote['creator_id']];?></p>
															<p class="alert alert-success"><?=nl2br(Sanitize::html($userNote['description']));?></p>
														</div>
													</div>
												</div>
											<? endforeach; ?>
										</div>
									<? else: ?>
										<div class="alert"><?=__('No Notes have been added yet');?></div>
									<? endif; ?>
									<div>
										<?=$this->Form->input(__('UserNote.summary'), array('class' => 'span5'));?>
										<?=$this->Form->input(__('UserNote.description'), array('class' => 'span5'));?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--<div class="tab-pane fade" id="settings">
						<?=__('Additional Settings');?>
					</div>-->
				</div>
			</div>
		</div>
	</div>
<?=$this->Form->end(); ?>