<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset(); ?>
	
	<title>
		<?=$pageTitle; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<?=$this->Html->meta('icon');?>
	<?=$this->fetch('meta');?>
	<?/* =$this->fetch('css'); */?>
	<?=$this->Html->css('bootstrap');?>
	<?=$this->Html->css('core');?>
	<?/* =$this->Html->css('fullcalendar'); */?>
</head>
<body>
	<?=$this->element('top-nav');?>
	<div id="content" class="container margin-top40">
		<?=$this->Session->flash(); ?>
		<div class="row">
			<div class="span12">
				<?=$this->fetch('content'); ?>
			</div>
		</div>
		<div id="footer">
			
		</div>
		<?=$this->Html->script('jquery.min');?>
		<?=$this->Html->script('bootstrap');?>
		<?=$this->fetch('script');?>
	</div>
</body>
</html>
