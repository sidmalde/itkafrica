<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset(); ?>
	<title>
		<?=$title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?=$this->Html->meta('icon');?>
	<?=$this->fetch('meta');?>
	<?=$this->Html->css('bootstrap');?>
	<?=$this->Html->css('core');?>
</head>
<body>
	<?/* =$this->element('navigation'); */?>
	<div id="content" class="container">
		<div class="row">
			<!--<div class="span3">
				<? if(!empty($currentUser)): ?>
					<?=$this->element('left-nav-admin');?>
				<? endif; ?>
			</div>-->
			<div class="span12">
				<div>
					<?=$this->Session->flash(); ?>

					<?=$this->fetch('content'); ?>
				</div>
			</div>
		</div>
		<div id="footer">
			
		</div>		
		<?=$this->Html->script('jquery.min');?>
		<?=$this->Html->script('bootstrap');?>
		<?=$this->Html->script('core');?>
		<?=$this->fetch('script');?>
	</div>
</body>
</html>
