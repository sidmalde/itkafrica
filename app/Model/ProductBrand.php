<?php
App::uses('AppModel', 'Model');

class ProductBrand extends AppModel {
	var $displayField = 'name';
	
	public $belongsTo = array(
		'SystemStatus'
	);
	public $hasMany = array(
		'Product',
	);
	
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
	);
}
