<?php
App::uses('AppModel', 'Model');

class SystemEvent extends AppModel {
	var $actsAs = array('Containable');
	
	public $belongsTo = array(
		'SystemModel',
		'SystemStatus',
	);
	
	public $hasMany = array(
		'SystemLog',
	);

}
