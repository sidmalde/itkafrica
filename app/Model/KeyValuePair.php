<?php
class KeyValuePair extends AppModel {
	var $name = 'KeyValuePair';
	
	function returnValue($key){
		return $this->field('value', array('KeyValuePair.key' => $key));	
	}
	
	function saveKey($key, $value){
		if($key && $value){
			$existing = $this->findByKey($key);
			if($existing){
				$existing['KeyValuePair']['value'] = $value;
				$existing['KeyValuePair']['modified'] = date("Y-m-d H:i:s");
				$this->save($existing);
			} else {
				$kvp = $this->create();
				$kvp['KeyValuePair']['key'] = $key;
				$kvp['KeyValuePair']['value'] = $value;
				$kvp['KeyValuePair']['modified'] = date("Y-m-d H:i:s");
				$this->save($kvp);
			}
			return true;		
		}	
		return false;
	}
	
	function deleteKey($key){
		if($key){
			$existing = $this->findByKey($key);
			if($existing){
				$this->delete($existing['KeyValuePair']['id']);
				return true;		
			}		
		}	
		return false;
	}
}
