<?php
App::uses('AppModel', 'Model');

class SystemTask extends AppModel {
	public $belongsTo = array(
		'SystemTicket',
	);
}
