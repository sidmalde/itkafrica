<?php

App::uses('Model', 'Model');

class AppModel extends Model {

	function addLog($eventId = null, $messageParams = array(), $urlParams = array(), $extraParams = array()){
		App::import('model', 'SystemLog');
		App::import('model', 'SystemEvent');
		
		$this->SystemLog = new SystemLog;
		$this->SystemEvent = new SystemEvent;
		
		$systemEvent = $this->SystemEvent->findById($eventId);
		
		$systemLog = $this->SystemLog->create();
		
		$systemLog['SystemLog']['system_event_id'] = $eventId;
		if (CakeSession::check('Auth.User.id')) {
			$log['SystemLog']['user_id'] = CakeSession::read('Auth.User.id');
		}
		$systemLog['SystemLog']['url'] = @vsprintf($systemEvent['SystemEvent']['url'], $urlParams);
		if (count($messageParams) == 0) {
			$messageParams[] = '';
		}
		$systemLog['SystemLog']['admin'] = $extraParams['SystemLog']['admin'];
		$systemLog['SystemLog']['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$systemLog['SystemLog']['description'] = vsprintf(__($systemEvent['SystemEvent']['description']), $messageParams);
		
		if($this->SystemLog->save($systemLog)){
			// Send the systemLog id to the notification function to process
			// $this->addNotification($this->SystemLog->id, $extraParams);
		}
		
		return false;
	}
}
