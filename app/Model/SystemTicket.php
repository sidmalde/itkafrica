<?php
App::uses('AppModel', 'Model');

class SystemTicket extends AppModel {

	public $belongsTo = array(
		'SystemMilestone',
		'Reporter' => array(
			'className' => 'user',
			'foreignKey' => 'reporter_id',
		),
		'SystemStatus'
	);
	
	public $hasMany = array(
		'SystemTask',
	);

}
