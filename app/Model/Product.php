<?php
App::uses('AppModel', 'Model');

class Product extends AppModel {
	var $displayField = 'name';
	
	public $belongsTo = array(
		'ProductBrand',
		'ProductCategory',
		'SystemStatus',
	);
	
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
		'product_category_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
		'system_status_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
	);
}
