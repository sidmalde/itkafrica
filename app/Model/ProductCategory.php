<?php
App::uses('AppModel', 'Model');

class ProductCategory extends AppModel {
	var $displayField = 'name';
	public $belongsTo = array(
		'ParentProductCategory' => array(
			'className' => 'ProductCategory',
			'foreignKey' => 'parent_product_category_id',
		),
		'SystemStatus',
	);
	
	 public $hasMany = array(
		'ProductSubCategory' => array(
			'className' => 'ProductCategory',
			'foreignKey' => 'parent_product_category_id',
		),
		/* 'SystemTicket',
		'Page',
		'Website', */
	);
	
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
	);
}
