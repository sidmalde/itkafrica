<?php
App::uses('AppModel', 'Model');
/**
 * PageHistory Model
 *
 * @property Page $Page
 * @property ParentPage $ParentPage
 * @property Website $Website
 * @property PageLayout $PageLayout
 */
class PageHistory extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Page' => array(
			'className' => 'Page',
			'foreignKey' => 'page_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ParentPage' => array(
			'className' => 'ParentPage',
			'foreignKey' => 'parent_page_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Website' => array(
			'className' => 'Website',
			'foreignKey' => 'website_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PageLayout' => array(
			'className' => 'PageLayout',
			'foreignKey' => 'page_layout_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
