<?php
App::uses('AppModel', 'Model');
/**
 * Page Model
 *
 * @property ParentPage $ParentPage
 * @property Website $Website
 * @property Status $Status
 * @property PageHistory $PageHistory
 */
class Page extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ParentPage' => array(
			'className' => 'Page',
			'foreignKey' => 'parent_page_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Website',
		'SystemStatus',
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'PageHistory' => array(
			'className' => 'PageHistory',
			'foreignKey' => 'page_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
