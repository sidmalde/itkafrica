<?php
App::uses('AppModel', 'Model');

class SystemModelLog extends AppModel {
	var $actsAs = array('Containable');
	
	public $belongsTo = array(
		'SystemModel',
		'Log',
	);
	
}