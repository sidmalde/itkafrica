<?php
App::uses('AppModel', 'Model');

class SystemStatus extends AppModel {
	var $actsAs = array('Containable');
	
	public $belongsTo = array(
		'SystemModel',
	);
	public $hasMany = array(
		'SystemEvent',
		'SystemTicket',
		'Page',
		'Website',
	);
	
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
	);
}
