<?php
App::uses('AppModel', 'Model');
/**
 * Log Model
 *
 * @property Event $Event
 */
class SystemLog extends AppModel {
	var $actsAs = array('Containable');
	
	public $belongsTo = array(
		'SystemEvent',
	);
	public $hasMany = array(
		'SystemModelLog',
	);
	
	public $validate = array(
		'admin' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
