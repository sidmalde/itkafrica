<?php
App::uses('AppModel', 'Model');
/**
 * ModelLog Model
 *
 * @property Model $Model
 * @property Log $Log
 */
class ModelLog extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Model' => array(
			'className' => 'Model',
			'foreignKey' => 'model_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Log' => array(
			'className' => 'Log',
			'foreignKey' => 'log_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
