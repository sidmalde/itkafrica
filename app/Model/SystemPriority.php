<?php
App::uses('AppModel', 'Model');

class SystemPriority extends AppModel {

	public $systemPriorities = array(
		1 => 'Blocker',
		2 => 'Critical',
		3 => 'Major',
		4 => 'Minor',
	);
}