<?php
App::uses('AppModel', 'Model');

class SystemModel extends AppModel {
	var $actsAs = array('Containable');
	
	public $hasMany = array(
		'SystemModelLog',
		'SystemEvent',
		'SystemStatus'
	);
	
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field and cannot be left empty',
			),
		),
	);
}
