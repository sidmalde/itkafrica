<?php
class Country extends AppModel {
	var $name = 'Country';
	var $order = 'position, name';
	var $actsAs =  array('Containable');
	var $displayField = 'name';
}
