<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'homepage'));
	Router::connect('/keepAlive/*', array('controller' => 'default', 'action' => 'keepalive'));
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/register', array('controller' => 'users', 'action' => 'register'));
	Router::connect('/rebuild-acl/sidmalde', array('controller' => 'Acl', 'action' => 'aco_aro_sync'));
	
	
	/*** Admin Routes ***/
	Router::connect('/dashboard', array('controller' => 'users', 'action' => 'dashboard', 'admin' => true));
	
	// Groups
	Router::connect('/system-management/groups', array('controller' => 'groups', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/groups/new', array('controller' => 'groups', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/groups/:group', array('controller' => 'groups', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/groups/:group/edit', array('controller' => 'groups', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/groups/:group/delete', array('controller' => 'groups', 'action' => 'delete', 'admin' => true));
	
	// Users
	Router::connect('/system-management/users', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/users/new', array('controller' => 'users', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/users/:user', array('controller' => 'users', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/users/:user/edit', array('controller' => 'users', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/users/:user/delete', array('controller' => 'users', 'action' => 'delete', 'admin' => true));
	
	// Pages
	Router::connect('/system-management/pages', array('controller' => 'pages', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/pages/new', array('controller' => 'pages', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/pages/:page', array('controller' => 'pages', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/pages/:page/edit', array('controller' => 'pages', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/pages/:page/delete', array('controller' => 'pages', 'action' => 'delete', 'admin' => true));
	
	// Tags
	Router::connect('/system-management/tags', array('controller' => 'tags', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/tags/new', array('controller' => 'tags', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/tags/:tag', array('controller' => 'tags', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/tags/:tag/edit', array('controller' => 'tags', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/tags/:tag/delete', array('controller' => 'tags', 'action' => 'delete', 'admin' => true));
	
	// Films
	Router::connect('/system-management/films', array('controller' => 'films', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/films/new', array('controller' => 'films', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/films/:film', array('controller' => 'films', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/films/:film/edit', array('controller' => 'films', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/films/:film/delete', array('controller' => 'films', 'action' => 'delete', 'admin' => true));
	
	// System Statuses
	Router::connect('/system-management/system-statuses', array('controller' => 'system_statuses', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/system-statuses/new', array('controller' => 'system_statuses', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/system-statuses/:systemStatus', array('controller' => 'system_statuses', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/system-statuses/:systemStatus/edit', array('controller' => 'system_statuses', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/system-statuses/:systemStatus/delete', array('controller' => 'system_statuses', 'action' => 'delete', 'admin' => true));
	
	// System Events
	Router::connect('/system-management/system-events', array('controller' => 'system_events', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/system-events/new', array('controller' => 'system_events', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/system-events/:systemEvent', array('controller' => 'system_events', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/system-events/:systemEvent/edit', array('controller' => 'system_events', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/system-events/:systemEvent/delete', array('controller' => 'system_events', 'action' => 'delete', 'admin' => true));
	
	// System Models
	Router::connect('/system-management/system-models', array('controller' => 'system_models', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/system-models/new', array('controller' => 'system_models', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/system-models/:systemModel', array('controller' => 'system_models', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/system-models/:systemModel/edit', array('controller' => 'system_models', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/system-models/:systemModel/delete', array('controller' => 'system_models', 'action' => 'delete', 'admin' => true));
	
	// System Logs
	Router::connect('/system-management/system-logs', array('controller' => 'system_logs', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/system-logs/new', array('controller' => 'system_logs', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/system-logs/:systemLog', array('controller' => 'system_logs', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/system-logs/:systemLog/edit', array('controller' => 'system_logs', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/system-logs/:systemLog/delete', array('controller' => 'system_logs', 'action' => 'delete', 'admin' => true));
	
	// System Milestones
	Router::connect('/system-management/system-milestones', array('controller' => 'system_milestones', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/system-milestones/new', array('controller' => 'system_milestones', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/system-milestones/:systemMilestone', array('controller' => 'system_milestones', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/system-milestones/:systemMilestone/edit', array('controller' => 'system_milestones', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/system-milestones/:systemMilestone/delete', array('controller' => 'system_milestones', 'action' => 'delete', 'admin' => true));
	
	Router::connect('/*', array('controller' => 'pages', 'action' => 'display'));

	
	CakePlugin::routes();
	require CAKE . 'Config' . DS . 'routes.php';
