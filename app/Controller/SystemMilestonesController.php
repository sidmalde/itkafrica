<?php
App::uses('AppController', 'Controller');

class SystemMilestonesController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'system-milestones');
		$this->layout = 'admin';
	}
	
	public function admin_index() {
		
	}
	
	public function admin_view() {
		
	}
	
	public function admin_add() {
		
	}
	
	public function admin_edit() {
		
	}
	
	public function admin_delete() {
		
	}
}
