<?php
App::uses('AppController', 'Controller');

class SystemModelsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'system-models');
		$this->layout = 'admin';
		$this->Auth->allow();
	}
	
	public function admin_index() {
		$this->SystemModel->contain();
		$systemModels = $this->SystemModel->find('all');
		
		$pageTitle = __('System Models');
		$this->set(compact(array('systemModels', 'pageTitle')));
	}
	
	public function admin_view() {
		if (empty($this->params['systemModel'])) {
			$this->Session->setFlash(__('Invalid System Model Id'), 'flash_failure');
			$this->redirect($this->referer);
		}
	}
	
	public function admin_add() {
		if (!empty($this->request->data)) {
			if ($this->SystemModel->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('System Model %s has been saved'), $this->request->data['SystemModel']['name']), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('System Model %s could not be saved, please try again'), $this->request->data['SystemModel']['name']), 'flash_success');
			}
		}
		
		$pageTitle = __('System Models &#187; New');
		$this->set(compact(array('pageTitle')));
	}
	
	public function admin_edit() {
		if (empty($this->params['systemModel'])) {
			$this->Session->setFlash(__('Invalid System Model Id'), 'flash_failure');
			$this->redirect($this->referer);
		}
		
		if (!empty($this->request->data)) {
			if ($this->SystemModel->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('System Model %s has been saved'), $this->request->data['SystemModel']['name']), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('System Model %s could not be saved, please try again'), $this->request->data['SystemModel']['name']), 'flash_success');
			}
		}
		
		$this->SystemModel->contain();
		$systemModel = $this->SystemModel->findById($this->params['systemModel']);
		// debug($systemModel);
		// die;
		$pageTitle = __('System Models &#187; New');
		$this->set(compact(array('pageTitle', 'systemModel')));
	}
	
	public function admin_delete() {
		if (empty($this->params['systemModel'])) {
			$this->Session->setFlash(__('Invalid System Model Id'), 'flash_failure');
			$this->redirect($this->referer);
		}
		$this->layout = false;
		
		if ($this->SystemModel->delete($this->params['systemModel'])) {
			$this->Session->setFlash(__('System Model deleted'), 'flash_success');
		} else {
			$this->Session->setFlash(__('System Model deleted'), 'flash_failure');
		}
		
		$this->redirect($this->referer());
	}
}