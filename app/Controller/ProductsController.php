<?php
App::uses('AppController', 'Controller');

class ProductsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'products');
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		// $this->Product->contain();
		$products = $this->Product->find('all');
		
		$pageTitle = __('Products');
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('New Product'),
				'url' => Router::url(array('action' => 'add', 'admin' => true)),
				'class' => 'btn btn-primary',
				'icon' => 'icon-white icon-plus-sign',
			),
		);
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'products')));
	}
	
	public function admin_view() {
		$this->layout = 'admin';
	
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		
		if (!empty($this->request->data)) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('Product has been saved.'), 'flash_success');
			} else {
				$this->Session->setFlash(__('Product could not be saved, please try again.'), 'flash_failure');
			}
		}
		
		$options = array(
			'conditions' => array(
				'SystemStatus.system_model_id' => '518416ac-5d00-45e2-9baf-0dac6352e1c8',
			),
		);
		$systemStatuses = $this->Product->SystemStatus->find('list', $options);
		$productCategories = $this->Product->ProductCategory->find('list');
		$productBrands = $this->Product->ProductBrand->find('list');
		
		$pageTitle = __('Products > New');
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('Cancel'),
				'url' => Router::url(array('action' => 'index', 'admin' => true)),
				'class' => 'btn btn-warning',
				'icon' => 'icon-white icon-arrow-left',
			),
		);
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'systemStatuses', 'productCategories', 'productBrands')));
	}
	
	public function admin_edit() {
		$this->layout = 'admin';
	
	}
	
	public function admin_delete() {
		$this->layout = 'admin';
	
	}
}