<?php
App::uses('AppController', 'Controller');
/**
 * Default Controller
 *
 * @property Default $Default
 */
class DefaultController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'default');
		$this->Auth->allow('');
	}
	
	
	public function oauth_2_callback() {
		$this->layout = false;
		debug($this->request->data);
		die;
		$this->redirect('/');
	}
	
	function keepAlive(){
		echo 0;
		exit();
	}
}