<?php
App::uses('AppController', 'Controller');

class SystemStatusesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
		$this->set('bodyClass', 'system-statuses');
		$this->layout = 'admin';
	}
	
	public function admin_index() {
		$systemStatuses = $this->SystemStatus->find('all');
		
		$pageTitle = __('System Statuses');
		$this->set(compact(array('pageTitle', 'systemStatuses')));
	}
	
	public function admin_view($id = null) {
		if (empty($this->params['systemStatus'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		$this->SystemStatus->contain();
		$options = array(
			'conditions' => array(
				'SystemStatus.id' => $this->params['systemStatus']
			)
		);
		$this->request->data = $systemStatus = $this->SystemStatus->find('first', $options);
		$systemModels = $this->SystemStatus->SystemModel->find('list');
		
		$pageTitle = __('System Status > %s > Edit', $systemStatus['SystemStatus']['name']);
		$this->set(compact(array('pageTitle', 'systemStatus', 'systemModels')));
	}
	
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SystemStatus->create();
			if ($this->SystemStatus->save($this->request->data)) {
				$this->Session->setFlash(__('The status has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The status could not be saved. Please, try again.'));
			}
		}
		$systemModels = $this->SystemStatus->SystemModel->find('list');
		$pageTitle = __('New System Status');
		$this->set(compact(array('pageTitle', 'systemModels')));
	}
	
	public function admin_edit($id = null) {
		if (empty($this->params['systemStatus'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		if (!empty($this->request->data)) {
			if ($this->SystemStatus->save($this->request->data)) {
				$this->Session->setFlash(__('The System Status has been updated.'), 'flash_success');
			} else {
				$this->Session->setFlash(__('The System Status could not be saved. Please, try again.'), 'flash_failure');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
	
	public function admin_delete($id = null) {
		if (empty($this->params['systemStatus'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		} else {
			$this->SystemStatus->id = $this->params['systemStatus'];
			if ($this->SystemStatus->delete()) {
				$this->Session->setFlash(__('System System Status has been deleted.'), 'flash_success');
				$this->redirect($this->referer());
			}
		}
		$this->Session->setFlash(__('System System Status could not be deleted, please try again.'), 'flash_failure');
		$this->redirect($this->referer());
	}
}
