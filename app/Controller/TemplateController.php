<?php
App::uses('AppController', 'Controller');

class TemplateController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'projects');
		$this->Auth->allow();
		$this->layout = 'default';
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		$modelObjects = $this->Model->find('all');
		$pageTitle = __('Project Types');
		$this->set(compact(array('pageTitle', 'modelObjects')));
	}
	
	public function admin_view() {
		$this->layout = 'admin';
		if (empty($this->params['action'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		$pageTitle = __('Model');
		$this->set(compact(array('pageTitle')));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		if (!empty($this->request->data)) {
			if ($this->Model->save($this->request->data)) {
				$this->Session->setFlash(__(''), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__(''), 'flash_failure');
			}
		}
		
		$pageTitle = __('New Model');
		$this->set(compact(array('pageTitle')));
	}
	
	public function admin_edit() {
		$this->layout = 'admin';
		if (empty($this->params['action'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		$pageTitle = __('Model');
		$this->set(compact(array('pageTitle')));
	}
	
	public function admin_delete() {
		$this->layout = 'admin';
		if (empty($this->params['action'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		$this->layout = false;
		
		if ($this->Model->delete($this->params['action'])) {
			;
		} else {
			$this->Session->setFlash(__('Unable to delete object, please try again'), 'flash_failure');
		}
		
		$this->redirect($this->referer());
	}
}