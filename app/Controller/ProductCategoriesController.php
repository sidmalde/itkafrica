<?php
App::uses('AppController', 'Controller');

class ProductCategoriesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'groups');
		$this->layout = 'default';
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		$options = array(
			'order' => array('ProductCategory.name' => 'ASC'),
		);
		$productCategories = $this->ProductCategory->find('all', $options);
		
		$pageTitle = __('Categories');
		$pageHeaderLinks[] = array(
			'label' => __('New Category'),
			'url' => Router::url(array('action' => 'add', 'admin' => true)),
			'class' => 'btn btn-primary',
			'icon' => 'icon-white icon-plus-sign'
		);
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'productCategories')));
	}
	
	public function admin_view() {
		$this->layout = 'admin';
		if (empty($this->params['productCategory'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		$this->request->data = $productCategory = $this->ProductCategory->findById($this->params['productCategory']);
		
		$pageTitle = __('Product Category > %s > Edit', $productCategory['ProductCategory']['name']);
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('New Category'),
				'url' => Router::url(array('action' => 'add', 'admin' => true)),
				'class' => 'btn btn-primary',
				'icon' => 'icon-white icon-plus-sign',
			),
			1 => array(
				'label' => __('Back'),
				'url' => Router::url(array('action' => 'index', 'admin' => true)),
				'class' => 'btn btn-warning',
				'icon' => 'icon-white icon-arrow-left',
			),
		);
		
		$parentProductCategories = $this->ProductCategory->find('list');
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'productCategory', 'parentProductCategories')));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		
		if (!empty($this->request->data)) {
			if ($this->ProductCategory->save($this->request->data)) {
				$this->Session->setFlash(__('Product category has been saved.'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Could not save Product Category, please try again.'), 'flash_failure');
			}
		}
		
		$pageTitle = __('Product Category > New');
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('Cancel'),
				'url' => Router::url(array('action' => 'index', 'admin' => true)),
				'class' => 'btn btn-warning',
				'icon' => 'icon-white icon-arrow-left',
			),
		);
		
		$parentProductCategories = $this->ProductCategory->find('list');
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'parentProductCategories')));
	}
	
	public function admin_edit() {
		$this->layout = 'admin';
		if (empty($this->params['productCategory'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		if (!empty($this->request->data)) {
			if ($this->ProductCategory->save($this->request->data)) {
				$this->Session->setFlash(__('Product Category has been updated.'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Could not save changes to Product Category, please try again.'), 'flash_failure');
			}
		}
	}
	
	public function admin_delete() {
		$this->layout = 'admin';
		if (empty($this->params['productCategory'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		if ($this->ProductCategory->delete($this->params['productCategory'])) {
			$this->Session->setFlash(__('Product Category has been deleted.'), 'flash_success');
			$this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('Product Category could not be deleted, please try again.'), 'flash_failure');
		}
	}
}