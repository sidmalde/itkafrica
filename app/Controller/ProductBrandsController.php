<?php
App::uses('AppController', 'Controller');

class ProductBrandsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'groups');
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		
		$productBrands = $this->ProductBrand->find('all');
		$pageTitle = __('Brands');
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('New Brand'),
				'url' => Router::url(array('action' => 'add', 'admin' => true)),
				'class' => 'btn btn-primary',
				'icon' => 'icon-white icon-plus-sign',
			),
		);
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'productBrands')));
	}
	
	public function admin_view() {
		if (empty($this->params['productBrand'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		$this->layout = 'admin';
		
		$this->request->data = $productBrand = $this->ProductBrand->findById($this->params['productBrand']);
		
		$options = array(
			'conditions' => array(
				'SystemStatus.system_model_id' => '518592ce-16cc-4087-9c1a-150c6352e1c8',
			),
		);
		$systemStatuses = $this->ProductBrand->SystemStatus->find('list', $options);
		
		$pageTitle = __('Brands > %s', $productBrand['ProductBrand']['name']);
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('New Brand'),
				'url' => Router::url(array('action' => 'add', 'admin' => true)),
				'class' => 'btn btn-primary',
				'icon' => 'icon-white icon-plus-sign',
			),
			1 => array(
				'label' => __('Back'),
				'url' => Router::url(array('action' => 'add', 'admin' => true)),
				'class' => 'btn btn-warning',
				'icon' => 'icon-white icon-arrow-left',
			),
		);
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'productBrand', 'systemStatuses')));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		
		if (!empty($this->request->data)) {
			if ($this->ProductBrand->save($this->request->data)) {
				$this->Session->setFlash(__('Brand has been saved.'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Brand could not be saved, please try again.'), 'flash_failure');
			}
		}
		
		$options = array(
			'conditions' => array(
				'SystemStatus.system_model_id' => '518592ce-16cc-4087-9c1a-150c6352e1c8',
			),
		);
		$systemStatuses = $this->ProductBrand->SystemStatus->find('list', $options);
		
		$pageTitle = __('Brands > New');
		$pageHeaderLinks = array(
			0 => array(
				'label' => __('Cancel'),
				'url' => Router::url(array('action' => 'index', 'admin' => true)),
				'class' => 'btn btn-warning',
				'icon' => 'icon-white icon-arrow-left',
			),
		);
		$this->set(compact(array('pageTitle', 'pageHeaderLinks', 'systemStatuses')));
	}
	
	public function admin_edit() {
		if (empty($this->params['productBrand'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		$this->layout = 'admin';
	
	}
	
	public function admin_delete() {
		if (empty($this->params['productBrand'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		$this->layout = false;
		
		$this->redirect($this->referer());
	}
}