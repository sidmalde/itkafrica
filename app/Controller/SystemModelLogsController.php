<?php
App::uses('AppController', 'Controller');
/**
 * ModelLogs Controller
 *
 * @property ModelLog $ModelLog
 */
class SystemModelLogsController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'system-model-logs');
		$this->layout = 'admin';
	}
}
