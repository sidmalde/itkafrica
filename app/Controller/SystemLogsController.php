<?php
App::uses('AppController', 'Controller');

class SystemLogsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'system-logs');
		$this->layout = 'admin';
		$this->Auth->allow();
	}
	
	public function admin_index () {
		$logs = $this->SystemLog->find('all');
		$pageTitle = __('All System Logs');
		$this->set(compact(array('logs', 'pageTitle')));
	}
	
	public function admin_view () {
		if (empty($this->params['log'])) {
			$this->Session->setFlash(__('Invalid Log'), 'flash_failure');
			$this->redirect($this->referer());
		}
	}
	
	public function admin_delete () {
		if (empty($this->params['log'])) {
			$this->Session->setFlash(__('Invalid Log'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		if ($this->Log->delete($this->params['log'])) {
			$this->Session->setFlash(__('Log successfully deleted'), 'flash_success');
		} else {
			$this->Session->setFlash(__('Log could not be deleted'), 'flash_failure');
		}
		$this->redirect($this->referer());
	}
}
