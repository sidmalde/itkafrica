<?php
App::uses('AppController', 'Controller');
/**
 * Tickets Controller
 *
 * @property SystemTicket $SystemTicket
 */
class SystemTicketsController extends AppController {
	
	public function index() {
		$this->SystemTicket->recursive = 0;
		$this->set('tickets', $this->paginate());
	}
	
	public function view($id = null) {
		if (!$this->SystemTicket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$options = array('conditions' => array('SystemTicket.' . $this->SystemTicket->primaryKey => $id));
		$this->set('ticket', $this->SystemTicket->find('first', $options));
	}
	
	public function add() {
		if ($this->request->is('post')) {
			$this->SystemTicket->create();
			if ($this->SystemTicket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		}
		$milestones = $this->SystemTicket->Milestone->find('list');
		$previousMilestones = $this->SystemTicket->PreviousMilestone->find('list');
		$reporters = $this->SystemTicket->Reporter->find('list');
		$managers = $this->SystemTicket->Manager->find('list');
		$priorities = $this->SystemTicket->Priority->find('list');
		$statuses = $this->SystemTicket->Status->find('list');
		$this->set(compact('milestones', 'previousMilestones', 'reporters', 'managers', 'priorities', 'statuses'));
	}
	
	public function edit($id = null) {
		if (!$this->SystemTicket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SystemTicket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SystemTicket.' . $this->SystemTicket->primaryKey => $id));
			$this->request->data = $this->SystemTicket->find('first', $options);
		}
		$milestones = $this->SystemTicket->Milestone->find('list');
		$previousMilestones = $this->SystemTicket->PreviousMilestone->find('list');
		$reporters = $this->SystemTicket->Reporter->find('list');
		$managers = $this->SystemTicket->Manager->find('list');
		$priorities = $this->SystemTicket->Priority->find('list');
		$statuses = $this->SystemTicket->Status->find('list');
		$this->set(compact('milestones', 'previousMilestones', 'reporters', 'managers', 'priorities', 'statuses'));
	}
	
	public function delete($id = null) {
		$this->SystemTicket->id = $id;
		if (!$this->SystemTicket->exists()) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->SystemTicket->delete()) {
			$this->Session->setFlash(__('SystemTicket deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('SystemTicket was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
