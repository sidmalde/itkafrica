<?php
class Upload extends AppModel {
	var $name = 'Upload';
	var $displayField = 'label';
	var $actsAs = array('Containable');
	
	/* var $hasAndBelongsToMany = array(
		'Emails' => array(
			'className' => 'EmailContent',
			'joinTable' => 'model_uploads',
			'foreignKey' => 'upload_id',
			'associationForeignKey' => 'model_id',
			'cascade' => true,
			'conditions' => array(
				'ModelUpload.model_name' => 'Emails'
			)
		)
	); */
	
	/* var $hasMany = array('Invoice'); */
	var $belongsTo = array('User');
}
