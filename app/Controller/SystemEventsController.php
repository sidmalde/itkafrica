<?php
App::uses('AppController', 'Controller');

class SystemEventsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'system-events');
		$this->layout = 'admin';
		$this->Auth->allow();
	}
	
	public function admin_index() {
		$options = array(
			'order' => 'SystemEvent.name ASC',
		);
		$events = $this->SystemEvent->find('all', $options);
		$pageTitle = __('Events');
		$this->set(compact(array('events', 'pageTitle')));
	}
	
	public function admin_view($id = null) {
		if (!$this->SystemEvent->exists($id)) {
			throw new NotFoundException(__('Invalid systemEvent'));
		}
		$options = array('conditions' => array('SystemEvent.' . $this->SystemEvent->primaryKey => $id));
		$systemEvent = $this->SystemEvent->find('first', $options);
		$pageTitle = sprintf(__('Events &#187; %s'), $systemEvent['SystemEvent']['name']);
		$this->set(compact(array('systemEvent', 'pageTitle')));
	}
	
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SystemEvent->create();
			if ($this->SystemEvent->save($this->request->data)) {
				$this->Session->setFlash(__('The systemEvent has been saved'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The systemEvent could not be saved. Please, try again.'), 'flash_failure');
			}
		}
		$systemModels = $this->SystemEvent->SystemModel->find('list');
		$options = array(
			'conditions' => array(
				'Status.status_type_id' => '513b87db-23cc-4b73-8993-07449699e514',
			),
		);
		$statuses = $this->SystemEvent->Status->find('list', $options);
		$pageTitle = __('Events &#187; New');
		$this->set(compact('systemModels', 'statuses', 'pageTitle'));
	}
	
	public function admin_edit() {
		if (empty($this->params['systemEvent'])) {
			$this->Session->setFlash(__('Invalid Id'), 'flash_failure');
			$this->redirect($this->referer());
		}
		if (!empty($this->request->data)) {
			if ($this->SystemEvent->save($this->request->data)) {
				$this->Session->setFlash(__('The systemEvent has been saved'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The systemEvent could not be saved. Please, try again.'), 'flash_failure');
			}
		}
		
		$options = array(
			'conditions' => array(
				'SystemEvent.id' => $this->params['systemEvent']
			)
		);
		$systemEvent = $this->request->data = $this->SystemEvent->find('first', $options);
		
		$systemModels = $this->SystemEvent->SystemModel->find('list');
		$options = array(
			'conditions' => array(
				'Status.status_type_id' => '513b87db-23cc-4b73-8993-07449699e514',
			),
		);
		$statuses = $this->SystemEvent->Status->find('list', $options);
		$pageTitle = sprintf(__('Events &#187; Edit &#187; %s'), $systemEvent['SystemEvent']['name']);
		$this->set(compact(array('systemModels', 'statuses', 'eventpageTitle', 'systemEvent', 'pageTitle')));
	}
	
	public function admin_delete($id = null) {
		if (empty($this->params['systemEvent'])) {
			$this->Session->setFlash(__('Invalid Id'), 'flash_failure');
			$this->redirect($this->referer());
		}
		if ($this->SystemEvent->delete($this->params['systemEvent'])) {
			$this->Session->setFlash(__('SystemEvent deleted'), 'flash_success');
		} else {
			$this->Session->setFlash(__('SystemEvent was not deleted'), 'flash_failure');
		}
		$this->redirect($this->referer());
	}
}
