<?php
App::uses('AppController', 'Controller');

class PagesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('homepage');
		$this->Auth->allow();
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
	}
	
	public function admin_view($id = null) {
		$this->layout = 'admin';
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
		$this->set('page', $this->Page->find('first', $options));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		if ($this->request->is('post')) {
			$this->Page->create();
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
		$parentPages = $this->Page->ParentPage->find('list');
		$websites = $this->Page->Website->find('list');
		$statuses = $this->Page->Status->find('list');
		$this->set(compact('parentPages', 'websites', 'statuses'));
	}
	
	public function admin_edit($id = null) {
		$this->layout = 'admin';
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
			$this->request->data = $this->Page->find('first', $options);
		}
		$parentPages = $this->Page->ParentPage->find('list');
		$websites = $this->Page->Website->find('list');
		$statuses = $this->Page->Status->find('list');
		$this->set(compact('parentPages', 'websites', 'statuses'));
	}
	
	public function admin_delete($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Page->delete()) {
			$this->Session->setFlash(__('Page deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function homepage() {
		$this->layout = 'homepage';
		
	}
}
