<?php
App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');

class AppController extends Controller {
	
	public $components = array(
		'Security',
		'Acl',
		'Auth' => array(
			'authorize' => array(
				'Actions' => array('actionPath' => 'controllers')
			),
			'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => null),
			'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email')
				)
			)
		),
		'Session',
		'Email',
		'DebugKit.Toolbar'
	);
	public $helpers = array('Html', 'Form', 'Session', /*'Javascript',*/ 'Number', 'Time', 'Text');
	
	function beforeFilter() {
		//Configure AuthComponent
		Security::setHash('md5');
		$this->Security->validatePost = false;
		$this->Security->csrfCheck = false;
		
		// $this->Auth->authorize = 'actions';
		$this->Auth->userScope = array('User.system_status_id' => ''); // The active flag must be set to 1 to be able to log into the website
		$this->Auth->autoRedirect = false;
		
		if ($this->Auth->user()) {
			$this->currentUser = $this->Auth->user();
			$this->set('currentUser', $this->currentUser);
		}
		
		//Configure AuthComponent
		$this->userTitles = $userTitles = array(
			'Mr' => 'Mr',
			'Mrs' => 'Mrs',
			'Miss' => 'Miss',
			'Dr' => 'Dr',
		);
		
		$this->allowedUploadExtensions = $allowedUploadExtensions = array(
			'doc' => 'doc',
			'docx' => 'docx',
			'xls' => 'xls',
			'xlsx' => 'xlsx',
			'pdf' => 'pdf',
			'jpg' => 'jpg',
			'jpeg' => 'jpeg',
			'gif' => 'gif',
			'png' => 'png',
			'tiff' => 'tiff',
		);
		$this->set(compact(array('userTitles', 'allowedUploadExtensions')));
	}
	
	function _checkAndUploadFile($folder, $file, $filename = null){
		
		App::import('Sanitize');
		if(!is_array($file)){
			return $file;
		} elseif($file['size']){
			if($filename){
				$file['name'] = $filename;
			} else {
				$file['name'] = basename(Sanitize::paranoid($file['name'],array('.', '-', '_')));
			}
			
			if (!file_exists('files/'.$folder)) {
				$pathToCreate = 'files/'.$folder;
				mkdir($pathToCreate, 0777, true);
			}
			
			move_uploaded_file($file['tmp_name'], 'files/'.$folder.'/'.$file['name']);
			return '/files/'.$folder.'/'.$file['name'];
		} else {
			return NULL;
		}
	}
	
	
	function generateRandomString($type = 'username', $length = 10) {    
		if ($type == 'username') {
			$string = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		} elseif ($type == 'password') {
			$string = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[]()!@\$^,.~|=-+_{}#";
		}
		return substr(str_shuffle($string), 0, $length);
	}
	
	function _object_to_array($obj) 
	{
		$arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
		foreach ($arrObj as $key => $val) {
			$val = (is_array($val) || is_object($val)) ? $this->object_to_array($val) : $val;
			$arr[$key] = $val;
		}
		return $arr;
	}
	
	function objectToArray($obj) 
	{
		$arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
		foreach ($arrObj as $key => $val) {
			if (is_array($val) || is_object($val)) {
				$val = $this->objectToArray($val);
			} else {
				$val = $val;
			}
			$arr[$key] = $val;
		}
		return $arr;
	}
	
	function arrayToObject($array) {
		$return = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$return->$k = $this->arrayToObject($v);
			}
			else {
				$return->$k = $v;
			}
		}
		return $return;
	}
	
	function differenceOf2Dates($datetime1, $datetime2, $options = null) {
		if (!empty($options)) {
		
		}
		
		$start = new DateTime($datetime1);
		$end = new DateTime($datetime2);
		$difference = $start->diff($end);
		
		return $difference;
	}
}
