<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login', 'logout');
		$this->set('bodyClass', 'users');
		$this->Auth->allow();
		$this->layout = 'default';
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		$this->User->Group->contain(array(
			'User'
		));
		$groups = $this->User->Group->find('all');
		
		$pageTitle = __('Users');
		$this->set(compact(array('pageTitle', 'groups')));
	}
	
	public function admin_view() {
		$this->layout = 'admin';
		if (empty($this->params['user'])) {
			$this->Session->setFlash(__('Invalid User Id'), 'flash_failure');
			$this->redirect('index');
		}
		
		$this->loadModel('Country');
		
		$this->Country->contain();
		$countries = $this->Country->find('list');
		$users = $this->User->find('list');
		$options = array(
			'conditions' => array(
				'User.id' => $this->params['user']
			)
		);
		$this->User->contain(array(
			'Group',
			'UserNote',
			'UserAttachment' => array(
				'conditions' => array(
					'UserAttachment.deleted' => 0,
				),
			),
		));
		$this->request->data = $user = $this->User->find('first', $options);
		
		if (empty($user)) {
			$this->Session->setFlash(__('Invalid User Id'), 'flash_failure');
			$this->redirect('index');
		}
		$groups = $this->User->Group->find('list');
		$pageTitle = sprintf(__('%ss &#187; %s'), $user['Group']['name'], $user['User']['fullnameNoTitle']);
		
		$this->set(compact(array('countries', 'users', 'user', 'groups', 'pageTitle')));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		if (!empty($this->request->data)) {
			$this->request->data['User']['active'] = true;
			$this->request->data['User']['deleted'] = false;
			
			// $this->request->data['User']['ref'] = $this->User->getRef($this->request->data['User']['group_id']);
			
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'flash_success');
				$this->redirect(array('action' => 'view', 'user' => $this->User->id));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash_failure');
			}
		}
		$groups = $this->User->Group->find('list');
		$pageTitle = __('System Maintenance &#187; Users &#187; New');
		$this->set(compact('groups', 'pageTitle'));
	}
	
	public function admin_edit($id = null) {
		$this->layout = 'admin';
		if (empty($this->params['user'])) {
			$this->Session->setFlash(__('Invalid User Id'), 'flash_failure');
			$this->redirect('dashboard');
		}
		
		$this->User->contain(array(
			'Group'
		));
		$options = array(
			'conditions' => array(
				'User.id' => $this->params['user'],
			)
		);
		
		$user = $this->User->find('first', $options);
		
		if (empty($user)) {
			$this->Session->setFlash(__('Invalid User Id'), 'flash_failure');
			$this->redirect('dashboard');
		}
		
		
		
		if (!empty($this->request->data)) {
			if (!empty($this->request->data['User']['attachment'])) {
				if(is_array($this->data['User']['attachment'])){
					if($this->data['User']['attachment']['error'] == 0){
						if(isset($this->data['User']['attachment']['tmp_name']) && isset($this->data['User']['attachment']['name'])){
							$file_parts = pathinfo($this->data['User']['attachment']['name']);
							if (in_array($file_parts['extension'], $this->allowedUploadExtensions)) {
								if($location = $this->_checkAndUploadFile('users/'.$user['User']['id'], $this->request->data['User']['attachment'])){
									$userAttachment = $this->User->UserAttachment->create();
									$userAttachment['UserAttachment']['system_model_name'] = 'User';
									$userAttachment['UserAttachment']['system_model_id'] = $user['User']['id'];
									$userAttachment['UserAttachment']['user_id'] = $this->Auth->user('id');
									$userAttachment['UserAttachment']['location'] = $location;
									$userAttachment['UserAttachment']['content_type'] =$this->request->data['User']['attachment']['type'];
									$userAttachment['UserAttachment']['filesize'] = $this->request->data['User']['attachment']['size'];
									$userAttachment['UserAttachment']['label'] = Sanitize::paranoid($this->request->data['User']['attachment']['name'],array('.', '-', '_'));
									$userAttachment['UserAttachment']['filename'] = $this->request->data['User']['attachment']['name'];
									$userAttachment['UserAttachment']['downloads'] = 0;
									$userAttachment['UserAttachment']['deleted'] = 0;
									$this->User->UserAttachment->save($userAttachment['UserAttachment']);
									$this->Session->setFlash(__('Attachment has been saved'), 'flash_success');
								} else {
									$this->Session->setFlash(__('Unable to save attachment'), 'flash_failure');
								}
							} else {
								$this->Session->setFlash(__('Invalid file format'), 'flash_failure');
							}
						}
					} else {
						$this->Session->setFlash(__('Unable to save attachment'), 'flash_failure');
					}
				}
			}
			
			if (!empty($this->request->data['UserNote']['summary'])) {
				$usernote = $this->User->UserNote->create();
				$userNote['creator_id'] = $this->Auth->user('id');
				$userNote['user_id'] = $this->request->data['User']['id'];
				$userNote['summary'] = $this->request->data['UserNote']['summary'];
				$userNote['description'] = $this->request->data['UserNote']['description'];
				$this->User->UserNote->save($userNote);
			}
			
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'flash_success');
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash_failure');
			}
		}
		
		$this->redirect(array('action' => 'view', 'user' => $user['User']['id']));
	}
	
	public function admin_delete($id = null) {
		$this->layout = false;
		if (empty($this->params['user'])) {
			$this->Session->setFlash(__('Invalid User Id'), 'flash_failure');
		}
		
		$this->User->contain();
		$user = $this->User->findById($this->params['user']);
		if (!empty($user)) {
			$user['User']['active'] = 0;
			$user['User']['deleted'] = 1;
			
			if ($this->User->save($user)) {
				$this->Session->setFlash(__('User deleted'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			}
		}
		$this->Session->setFlash(__('User was not deleted'), 'flash_failure');
		$this->redirect($this->referer());
	}
	
	public function login() {
		$this->layout = 'login';
		$pageTitle = __('Login');
		if ($this->Auth->user()) {
			if ($this->Auth->user('group_id') == '51488314-33c4-4394-8d02-0f0c46dad844') {
				$this->redirect('/dashboard');
			} else {
				$this->redirect('/myDashboard');
			}
		} else {
			if ($this->request->is('post')) {
				if ($this->Auth->login()) {
					/* $messageParams = array(
						$this->Auth->user('fullnameNoTitle'),
						date("d-M-Y H:i"),
						$_SERVER['REMOTE_ADDR'],
					);
					$urlParams = array(
						$this->Auth->user('id')
					);
					$extraParams = array(
						'SystemLog' => array(
							'admin' => true,
						),
					);
					$this->User->addLog('516c5d04-efc4-43a8-bec7-0eb846dad844', $messageParams, $urlParams, $extraParams); */
					if ($this->Auth->user('group_id') == '51488314-33c4-4394-8d02-0f0c46dad844') {
						$this->redirect('/dashboard');
					} else {
						$this->redirect('/myDashboard');
					}
				} else {
					$this->Session->setFlash(__('Your username or password was incorrect, please try again.'), 'flash_failure');
				}
			}
		}
		$this->set(compact(array('pageTitle')));
	}

	public function logout() {
		$messageParams = array(
			$this->Auth->user('fullnameNoTitle'),
			date("d-M-Y H:i"),
			$_SERVER['REMOTE_ADDR'],
		);
		$urlParams = array(
			$this->Auth->user('id')
		);
		$extraParams = array(
			'SystemLog' => array(
				'admin' => true,
			),
		);
		$this->User->addLog('516c5d4e-2680-4e20-92e8-0eb846dad844', $messageParams, $urlParams, $extraParams);
		$this->Session->setFlash(__('Thank you for choosing Guru Solutions'), 'flash_success');
		$this->redirect($this->Auth->logout());
	}
	
	public function admin_dashboard() {
		$this->layout = 'admin';
		if (!empty($this->params['tab'])) {
			$this->set('selectedTab', $this->params['tab']);
		} else {
			$this->set('selectedTab', 'logs');
		}
		$this->loadModel('SystemLog');
		$logStartDate = date("Y-m-d", strtotime(date("Y-m-d").' - 15 days'));
		$options = array(
			'conditions' => array(
				'SystemLog.created >' => $logStartDate
			),
			'order' => array(
				'SystemLog.created' => 'DESC'
			),
		);
		$systemLogs = $this->SystemLog->find('all', $options);
		$this->request->data['User'] = $this->currentUser;
		$pageTitle = __('Dashboard');
		$bodyClass = 'dashboard';
		$this->set(compact(array('pageTitle', 'bodyClass', 'systemLogs')));
	}
	
	public function my_dashboard() {
		$pageTitle = __('My Dashboard');
		$bodyClass = 'dashboard';
		$this->set(compact(array('pageTitle', 'bodyClass')));
	}
	
	public function admin_delete_attachment() {
		if (empty($this->params['userAttachment'])) {
			$this->Session->setFlash(__('Invalid User Attachment Id'), 'flash_failure');
			$this->redirect('index');
		}
		
		$this->User->UserAttachment->contain();
		$userAttachment = $this->User->UserAttachment->findById($this->params['userAttachment']);;
		$userAttachment['UserAttachment']['deleted'] = 1;
		$this->User->UserAttachment->id = $this->params['userAttachment'];
		if ($this->User->UserAttachment->save($userAttachment)) {
			$this->Session->setFlash(__('User Attachment deleted'), 'flash_success');
		} else {
			$this->Session->setFlash(__('User Attachment was not deleted'), 'flash_failure');
		}
		$this->redirect(array('action' => 'view', 'user' => $userAttachment['UserAttachment']['model_id']));
	}
}
