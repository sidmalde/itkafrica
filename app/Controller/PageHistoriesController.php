<?php
App::uses('AppController', 'Controller');
/**
 * PageHistories Controller
 *
 * @property PageHistory $PageHistory
 */
class PageHistoriesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PageHistory->recursive = 0;
		$this->set('pageHistories', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PageHistory->exists($id)) {
			throw new NotFoundException(__('Invalid page history'));
		}
		$options = array('conditions' => array('PageHistory.' . $this->PageHistory->primaryKey => $id));
		$this->set('pageHistory', $this->PageHistory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PageHistory->create();
			if ($this->PageHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The page history has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page history could not be saved. Please, try again.'));
			}
		}
		$pages = $this->PageHistory->Page->find('list');
		$parentPages = $this->PageHistory->ParentPage->find('list');
		$websites = $this->PageHistory->Website->find('list');
		$pageLayouts = $this->PageHistory->PageLayout->find('list');
		$this->set(compact('pages', 'parentPages', 'websites', 'pageLayouts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PageHistory->exists($id)) {
			throw new NotFoundException(__('Invalid page history'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PageHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The page history has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PageHistory.' . $this->PageHistory->primaryKey => $id));
			$this->request->data = $this->PageHistory->find('first', $options);
		}
		$pages = $this->PageHistory->Page->find('list');
		$parentPages = $this->PageHistory->ParentPage->find('list');
		$websites = $this->PageHistory->Website->find('list');
		$pageLayouts = $this->PageHistory->PageLayout->find('list');
		$this->set(compact('pages', 'parentPages', 'websites', 'pageLayouts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PageHistory->id = $id;
		if (!$this->PageHistory->exists()) {
			throw new NotFoundException(__('Invalid page history'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PageHistory->delete()) {
			$this->Session->setFlash(__('Page history deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page history was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
