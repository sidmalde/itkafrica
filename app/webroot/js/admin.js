var $waitingCount = 0;
var $keepAliveCount = 0;
var $keepAlive;

function keepAlive(){
	$.get('/keepAlive/'+Math.random(), function(){});
	$keepAlive = setTimeout("keepAlive()",600000); // once every 5 minutes for 2 hours
	/* if($keepAliveCount < 24){
	} else {
		clearTimeout($keepAlive);
	}
	$keepAliveCount++; */
}

$(document).ready(function(){
	// fc-event-inner
	keepAlive();
	
	// Flash messages in header
	if($('#flashMessage').length > 0) {
		$('#flashMessage').show();
		$('#flashMessage').animate({
			opacity: 1,
			'top': '-=10px'
		}, 500);
		$('#flashMessage').delay(10000).fadeOut(400);
	}
	
	$('a[data-toggle="tab"]').on('shown', function (e) {
	  e.target // activated tab
	  e.relatedTarget // previous tab
	})
	
	$('#myTab a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})
	
	nicEditors.allTextAreas({
		fullPanel	:	true,
		/* buttonList	:	['',''], */
		buttonList	:	['',''],
		iconsPath	:	'/img/nicEdit/nicEditorIcons.gif',
		maxHeight	:	350,
	});
	
	$('input.input-file-real').change(function() {
		$fakeInput = $(this).data('fake-input');
		$('#'+$fakeInput).val($(this).val().replace("C:\\fakepath\\", ""));
	});
	
	$('.input-file-real-clear').click(function() {
		$realInput = $(this).data('real-input');
		$fakeInput = $(this).data('fake-input');
		$('#'+$realInput).val('');
		$('#'+$fakeInput).val('');
	});
	
	/* new nicEditor().panelInstance('area1');
	new nicEditor({fullPanel : true}).panelInstance('area2');
	new nicEditor({iconsPath : '/img/nicEdit/nicEditorIcons.gif'}).panelInstance('area3');
	new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
	new nicEditor({maxHeight : 100}).panelInstance('area5'); */
	
});