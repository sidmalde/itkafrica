$(document).ready(function(){
	$('a[data-toggle="tab"]').on('shown', function (e) {
	  e.target // activated tab
	  e.relatedTarget // previous tab
	})
	
	$('#myTab a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})

	$('input.input-file-real').change(function() {
		$fakeInput = $(this).data('fake-input');
		$('#'+$fakeInput).val($(this).val().replace("C:\\fakepath\\", ""));
	});
	
	$('.input-file-real-clear').click(function() {
		$realInput = $(this).data('real-input');
		$fakeInput = $(this).data('fake-input');
		$('#'+$realInput).val('');
		$('#'+$fakeInput).val('');
	});
	
	// $('#myTab a[href="#profile"]').tab('show'); // Select tab by name
	// $('#myTab a:first').tab('show'); // Select first tab
	// $('#myTab a:last').tab('show'); // Select last tab
	// $('#myTab li:eq(2) a').tab('show'); // Select third tab (0-indexed)
	
});