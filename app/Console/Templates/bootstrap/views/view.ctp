<div class="<?php echo $pluralVar; ?> view">
	<?php echo "<?php echo \$this->Html->link('<i class=\"icon-arrow-left\"></i> Return', array('action' => 'index'), array('class' => 'pull-right btn', 'escape' => false)); ?>"; ?>
  <h2><?php echo "<?php  echo __('{$singularHumanName}'); ?>"; ?></h2>
  <dl class="dl-horizontal">
  <?php
 	$excludeFields = array('id', 'created');	
  foreach ($fields as $field) {
    if(!in_array($field, $excludeFields)):
			$isKey = false;
			if (!empty($associations['belongsTo'])) {
				foreach ($associations['belongsTo'] as $alias => $details) {
					if ($field === $details['foreignKey']) {
						$isKey = true;
						echo "\t\t<dt><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></dt>\n";
						echo "\t\t<dd>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
						break;
					}
				}
			}
			if ($isKey !== true) {
				echo "\t\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
				if($field == 'modified'){
					echo "\t\t<dd>\n\t\t\t<?php echo \$this->Time->niceShort(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
				} else {
					echo "\t\t<dd>\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
				}
			}
		endif; // exclude fields
  }
  ?>
	</dl>
</div>
<?php
if (!empty($associations['hasOne'])) :
	foreach ($associations['hasOne'] as $alias => $details): ?>
	<div class="related">
		<h3><?php echo "<?php echo __('Related " . Inflector::humanize($details['controller']) . "'); ?>"; ?></h3>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
		<dl>
	<?php
			foreach ($details['fields'] as $field) {
				echo "\t\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
				echo "\t\t<dd>\n\t<?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n&nbsp;</dd>\n";
			}
	?>
		</dl>
	<?php echo "<?php endif; ?>\n"; ?>
		<div class="actions">
			<ul>
				<li><?php echo "<?php echo \$this->Html->link(__('Edit " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?></li>\n"; ?>
			</ul>
		</div>
	</div>
	<?php
	endforeach;
endif;
if (empty($associations['hasMany'])) {
	$associations['hasMany'] = array();
}
if (empty($associations['hasAndBelongsToMany'])) {
	$associations['hasAndBelongsToMany'] = array();
}
$relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
$i = 0;
foreach ($relations as $alias => $details):
	$otherSingularVar = Inflector::variable($alias);
	$otherPluralHumanName = Inflector::humanize($details['controller']);
	?>
<div class="well">
  <?php echo "<?php echo \$this->Html->link(__('<i class=\"icon-plus icon-white\"></i> New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'btn btn-success pull-right btn-small', 'escape' => false)); ?>"; ?> 
	<h3><?php echo "<?php echo __('".$otherPluralHumanName."'); ?>"; ?></h3>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
	<table class="table table-striped table-bordered table-condensed">
  	<thead>
      <tr>
        <?php
              foreach ($details['fields'] as $field) {
                if(!in_array($field, $excludeFields)):
                  echo "\t\t<th><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
                endif; 
              }
        ?>
      </tr>
		</thead>
    <tbody>
				<?php
        echo "\t<?php
            \$i = 0;
            foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}): ?>\n";
            echo "\t\t<tr>\n";
              foreach ($details['fields'] as $field) {
                if(!in_array($field, $excludeFields)):
                  switch($field){
                    case 'modified':
                      echo "\t\t<td><?php echo \$this->Time->niceShort(\${$otherSingularVar}['{$field}']); ?></td>\n";
                    break;
                    case 'publish':
                      echo "\t\t<td><?php echo (\${$otherSingularVar}['{$field}']) ? 'Pubished' : 'Un-published'; ?></td>\n";
                    break;
                    case 'active':
                      echo "\t\t<td><?php echo (\${$otherSingularVar}['{$field}']) ? 'Active' : 'Inactive'; ?></td>\n";
                    break;
                    
                    default:
                      echo "\t\t<td><?php echo h(\${$otherSingularVar}['{$field}']); ?>&nbsp;</td>\n";
                    break;
                  } 
                endif; // exclude fields
              }
            echo "\t\t</tr>\n";
        
        echo "\t<?php endforeach; ?>\n";
        ?>
			</tbody>
    </table>
  <?php echo "<?php endif; ?>\n\n"; ?>
</div>
<?php endforeach; ?>
