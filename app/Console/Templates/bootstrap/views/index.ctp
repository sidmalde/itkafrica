<h1><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <tr>
      <?php $excludeFields = array('id', 'created'); ?>
      <?php foreach ($fields as $field): ?>
        <?php if(!in_array($field, $excludeFields)): ?>
          <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
        <?php endif; ?>
      <?php endforeach; ?>
      <th><?php echo "<?php echo \$this->Html->link(__('<i class=\"icon-plus\"></i> New " . $singularHumanName . "'), array('action' => 'add'), array('class' => 'btn btn-success btn-small', 'escape' => false)); ?>"; ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
    echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
    echo "\t<tr>\n";
      foreach ($fields as $field) {
        if(!in_array($field, $excludeFields)):
          $isKey = false;
          if (!empty($associations['belongsTo'])) {
            foreach ($associations['belongsTo'] as $alias => $details) {
              if ($field === $details['foreignKey']) {
                $isKey = true;
                echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
                break;
              }
            }
          }
          if ($isKey !== true) {
            switch($field){
              case 'modified':
                echo "\t\t<td><?php echo \$this->Time->niceShort(\${$singularVar}['{$modelClass}']['{$field}']); ?></td>\n";
              break;
              case 'publish':
                echo "\t\t<td><?php echo (\${$singularVar}['{$modelClass}']['{$field}']) ? 'Pubished' : 'Un-published'; ?></td>\n";
              break;
              case 'active':
                echo "\t\t<td><?php echo (\${$singularVar}['{$modelClass}']['{$field}']) ? 'Active' : 'Inactive'; ?></td>\n";
              break;
              
              default:
                echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
              break;
            } 
          }
        endif; // end exclude fields in_array
      }
  
      echo "\t\t<td>\n";
      echo "\t\t\t<div class=\"btn-group\">\n";
      echo "\t\t\t\t<?php echo \$this->Html->link(__('View'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-small')); ?>\n";
      echo "\t\t\t\t<?php echo \$this->Html->link(__('<i class=\"icon-pencil\"></i> Edit'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-small', 'escape' => false)); ?>\n";
      echo "\t\t\t\t<?php echo \$this->Form->postLink(__('<i class=\"icon-trash icon-white\"></i> Delete'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-danger btn-small', 'escape' => false), 'Are you sure you want to delete this {$singularVar}?'); ?>\n";
      echo "\t\t\t</div>\n";
      echo "\t\t</td>\n";
    echo "\t</tr>\n";
  
    echo "<?php endforeach; ?>\n";
    ?>
  </tbody>
</table>
<?php echo "<?php if (\$this->Paginator->numbers()): ?>"; ?>
<div class="pagination pagination-centered">
  <?php
    echo "<?php\n";
    echo "\t\techo \$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));\n";
    echo "\t\techo \$this->Paginator->numbers(array('separator' => ''));\n";
    echo "\t\techo \$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));\n";
    echo "\t?>\n";
  ?>
</div>
<?php echo "<?php endif; ?>"; ?>