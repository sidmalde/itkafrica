<?php echo "<?php echo \$this->Form->create('{$modelClass}', array('type' => 'file')); ?>\n"; ?>
	<fieldset>
		<legend><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></legend>
<?php
		echo "\t<?php\n";
		foreach ($fields as $field) {
			switch($field):
				case 'image': case 'file': case 'filename': case 'image_thumb': case 'document':
					if (strpos($action, 'add') !== false){ // add
						echo "\t\techo \$this->Form->hidden('{$field}');\n";
						echo "\t\techo \$this->Html->image(AmazonUrl\.\$this->request->data['{$modeClass}']['{$field}']);";
						echo "\t\techo \$this->Html->link(__('Delete {$field}'), array('action' => 'delete_field', $this->request->data['Floorplan']['id'], 'thumbnail'), array('class' => 'btn btn-danger'), 'Are you sure you want to delete this {$image}?');";
					} else { // edit
						echo "\t\techo \$this->Form->input('{$field}', array('type' => 'file'));\n";
					}
				break;
				default:
					if (strpos($action, 'add') !== false && $field == $primaryKey) {
						continue;
					} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
						echo "\t\techo \$this->Form->input('{$field}', array('class' => 'input-block-level', 'placeholder' => '{$field}'));\n";
					}
				break;
			endswitch;
		}
		if (!empty($associations['hasAndBelongsToMany'])) {
			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
				echo "\t\techo \$this->Form->input('{$assocName}');\n";
			}
		}
		echo "\t?>\n";
?>
   	<div class="btn-group">
      <button class="btn btn-success" type="submit" data-loading-text="Saving...">Save</button>
      <?php echo "<?php echo \$this->Html->link('Cancel', array('action' => 'index'), array('class' => 'btn btn-danger'), 'Are you sure you want to cancel?'); ?>"; ?>
		</div>
	</fieldset>
<?php
	echo "<?php echo \$this->Form->end(); ?>\n";
?>