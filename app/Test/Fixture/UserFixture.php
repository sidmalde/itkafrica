<?php
/**
 * UserFixture
 *
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary', 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'group_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 36, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'password' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 120, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'title' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 8, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'firstname' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 120, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'lastname' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 120, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'gender' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'date_of_birth' => array('type' => 'date', 'null' => true, 'default' => null),
		'phone' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 13, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'mobile' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 13, 'collate' => 'ascii_general_ci', 'charset' => 'ascii'),
		'active' => array('type' => 'boolean', 'null' => true, 'default' => '1'),
		'deleted' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '514249d4-a234-4aef-9f1c-0d5046dad844',
			'group_id' => 'Lorem ipsum dolor sit amet',
			'email' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'title' => 'Lorem ',
			'firstname' => 'Lorem ipsum dolor sit amet',
			'lastname' => 'Lorem ipsum dolor sit amet',
			'gender' => 'Lorem ip',
			'date_of_birth' => '2013-03-14',
			'phone' => 'Lorem ipsum',
			'mobile' => 'Lorem ipsum',
			'active' => 1,
			'deleted' => 1,
			'created' => '2013-03-14 22:06:12',
			'modified' => '2013-03-14 22:06:12'
		),
	);

}
