<?php
/**
 * PageHistoryFixture
 *
 */
class PageHistoryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'length' => 36, 'key' => 'primary', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'page_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 36, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'parent_page_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 36, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'website_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 36, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'page_layout_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 36, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dpr' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'label' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'title' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'url' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'content' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'publish' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'moveable' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'edit_page_layout' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'edit_dpr' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'edit_url' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'edit_content' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'deletable' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'children' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'position' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 2),
		'expires' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'seo_priority' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '3,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '51424951-a594-4c17-808e-1e4446dad844',
			'page_id' => 'Lorem ipsum dolor sit amet',
			'parent_page_id' => 'Lorem ipsum dolor sit amet',
			'website_id' => 'Lorem ipsum dolor sit amet',
			'page_layout_id' => 'Lorem ipsum dolor sit amet',
			'dpr' => 'Lorem ipsum dolor sit amet',
			'label' => 'Lorem ipsum dolor sit amet',
			'title' => 'Lorem ipsum dolor sit amet',
			'url' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'content' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'publish' => 1,
			'moveable' => 1,
			'edit_page_layout' => 1,
			'edit_dpr' => 1,
			'edit_url' => 1,
			'edit_content' => 1,
			'deletable' => 1,
			'children' => 1,
			'position' => 1,
			'expires' => '2013-03-14 22:04:01',
			'seo_priority' => 1,
			'created' => '2013-03-14 22:04:01',
			'modified' => '2013-03-14 22:04:01'
		),
	);

}
