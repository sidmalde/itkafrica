<?php
App::uses('StatusType', 'Model');

/**
 * StatusType Test Case
 *
 */
class StatusTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.status_type',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StatusType = ClassRegistry::init('StatusType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StatusType);

		parent::tearDown();
	}

}
