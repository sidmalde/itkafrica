<?php
App::uses('PageHistory', 'Model');

/**
 * PageHistory Test Case
 *
 */
class PageHistoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.page_history',
		'app.page',
		'app.parent_page',
		'app.website',
		'app.page_layout'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PageHistory = ClassRegistry::init('PageHistory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PageHistory);

		parent::tearDown();
	}

}
