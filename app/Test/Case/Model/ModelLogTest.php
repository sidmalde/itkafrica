<?php
App::uses('ModelLog', 'Model');

/**
 * ModelLog Test Case
 *
 */
class ModelLogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.model_log',
		'app.model',
		'app.log',
		'app.event',
		'app.event_type',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ModelLog = ClassRegistry::init('ModelLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ModelLog);

		parent::tearDown();
	}

}
