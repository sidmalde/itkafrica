<?php
App::uses('Website', 'Model');

/**
 * Website Test Case
 *
 */
class WebsiteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.website',
		'app.status',
		'app.status_type',
		'app.event',
		'app.event_type',
		'app.log',
		'app.page',
		'app.parent_page',
		'app.page_history',
		'app.page_layout',
		'app.ticket',
		'app.milestone',
		'app.previous_milestone',
		'app.reporter',
		'app.manager',
		'app.priority',
		'app.task',
		'app.assigned',
		'app.adwords'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Website = ClassRegistry::init('Website');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Website);

		parent::tearDown();
	}

}
