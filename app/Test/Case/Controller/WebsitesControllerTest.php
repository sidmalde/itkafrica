<?php
App::uses('WebsitesController', 'Controller');

/**
 * WebsitesController Test Case
 *
 */
class WebsitesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.website',
		'app.status',
		'app.status_type',
		'app.event',
		'app.event_type',
		'app.log',
		'app.page',
		'app.parent_page',
		'app.page_history',
		'app.page_layout',
		'app.ticket',
		'app.milestone',
		'app.previous_milestone',
		'app.reporter',
		'app.manager',
		'app.priority',
		'app.task',
		'app.assigned'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
