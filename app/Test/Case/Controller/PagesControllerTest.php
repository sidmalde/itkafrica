<?php
App::uses('PagesController', 'Controller');

/**
 * PagesController Test Case
 *
 */
class PagesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.page',
		'app.website',
		'app.status',
		'app.status_type',
		'app.event',
		'app.event_type',
		'app.log',
		'app.ticket',
		'app.milestone',
		'app.previous_milestone',
		'app.reporter',
		'app.manager',
		'app.priority',
		'app.task',
		'app.assigned',
		'app.page_history',
		'app.page_layout'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
