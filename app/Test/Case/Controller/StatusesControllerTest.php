<?php
App::uses('StatusesController', 'Controller');

/**
 * StatusesController Test Case
 *
 */
class StatusesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.status',
		'app.status_type',
		'app.event',
		'app.event_type',
		'app.log',
		'app.page',
		'app.parent_page',
		'app.website',
		'app.page_history',
		'app.page_layout',
		'app.ticket'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
