/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : sidmalde_darksteel

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-05-02 21:04:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` char(36) DEFAULT NULL,
  `parent_account_id` char(36) DEFAULT NULL,
  `system_status_id` char(36) DEFAULT NULL,
  `ref` varchar(12) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `reseller` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accounts
-- ----------------------------

-- ----------------------------
-- Table structure for `acos`
-- ----------------------------
DROP TABLE IF EXISTS `acos`;
CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`) USING BTREE,
  KEY `idx_acos_alias` (`alias`) USING BTREE,
  KEY `idx_acos_model_foreign_key` (`model`,`foreign_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acos
-- ----------------------------
INSERT INTO `acos` VALUES ('1', null, null, null, 'controllers', '1', '198');
INSERT INTO `acos` VALUES ('2', '1', null, null, 'Acl', '2', '17');
INSERT INTO `acos` VALUES ('3', '2', null, null, 'startup', '3', '4');
INSERT INTO `acos` VALUES ('4', '2', null, null, 'aco_aro_sync', '5', '6');
INSERT INTO `acos` VALUES ('5', '2', null, null, 'aco_update', '7', '8');
INSERT INTO `acos` VALUES ('6', '2', null, null, 'getControllerList', '9', '10');
INSERT INTO `acos` VALUES ('7', '2', null, null, 'verify', '11', '12');
INSERT INTO `acos` VALUES ('8', '2', null, null, 'recover', '13', '14');
INSERT INTO `acos` VALUES ('9', '2', null, null, 'init_group_permissions', '15', '16');
INSERT INTO `acos` VALUES ('22', '1', null, null, 'Groups', '18', '29');
INSERT INTO `acos` VALUES ('46', '1', null, null, 'PageHistories', '30', '41');
INSERT INTO `acos` VALUES ('47', '46', null, null, 'index', '31', '32');
INSERT INTO `acos` VALUES ('48', '46', null, null, 'view', '33', '34');
INSERT INTO `acos` VALUES ('49', '46', null, null, 'add', '35', '36');
INSERT INTO `acos` VALUES ('50', '46', null, null, 'edit', '37', '38');
INSERT INTO `acos` VALUES ('51', '46', null, null, 'delete', '39', '40');
INSERT INTO `acos` VALUES ('52', '1', null, null, 'Pages', '42', '53');
INSERT INTO `acos` VALUES ('53', '52', null, null, 'index', '43', '44');
INSERT INTO `acos` VALUES ('54', '52', null, null, 'view', '45', '46');
INSERT INTO `acos` VALUES ('55', '52', null, null, 'add', '47', '48');
INSERT INTO `acos` VALUES ('56', '52', null, null, 'edit', '49', '50');
INSERT INTO `acos` VALUES ('57', '52', null, null, 'delete', '51', '52');
INSERT INTO `acos` VALUES ('82', '1', null, null, 'Users', '54', '77');
INSERT INTO `acos` VALUES ('88', '82', null, null, 'login', '55', '56');
INSERT INTO `acos` VALUES ('89', '82', null, null, 'logout', '57', '58');
INSERT INTO `acos` VALUES ('90', '1', null, null, 'Websites', '78', '89');
INSERT INTO `acos` VALUES ('91', '90', null, null, 'index', '79', '80');
INSERT INTO `acos` VALUES ('92', '90', null, null, 'view', '81', '82');
INSERT INTO `acos` VALUES ('93', '90', null, null, 'add', '83', '84');
INSERT INTO `acos` VALUES ('94', '90', null, null, 'edit', '85', '86');
INSERT INTO `acos` VALUES ('95', '90', null, null, 'delete', '87', '88');
INSERT INTO `acos` VALUES ('96', '1', null, null, 'AclExtras', '90', '91');
INSERT INTO `acos` VALUES ('97', '1', null, null, 'DebugKit', '92', '99');
INSERT INTO `acos` VALUES ('98', '97', null, null, 'ToolbarAccess', '93', '98');
INSERT INTO `acos` VALUES ('99', '98', null, null, 'history_state', '94', '95');
INSERT INTO `acos` VALUES ('100', '98', null, null, 'sql_explain', '96', '97');
INSERT INTO `acos` VALUES ('101', '22', null, null, 'admin_index', '19', '20');
INSERT INTO `acos` VALUES ('102', '22', null, null, 'admin_view', '21', '22');
INSERT INTO `acos` VALUES ('103', '22', null, null, 'admin_add', '23', '24');
INSERT INTO `acos` VALUES ('104', '22', null, null, 'admin_edit', '25', '26');
INSERT INTO `acos` VALUES ('105', '22', null, null, 'admin_delete', '27', '28');
INSERT INTO `acos` VALUES ('106', '82', null, null, 'admin_index', '59', '60');
INSERT INTO `acos` VALUES ('107', '82', null, null, 'admin_view', '61', '62');
INSERT INTO `acos` VALUES ('108', '82', null, null, 'admin_add', '63', '64');
INSERT INTO `acos` VALUES ('109', '82', null, null, 'admin_edit', '65', '66');
INSERT INTO `acos` VALUES ('110', '82', null, null, 'admin_delete', '67', '68');
INSERT INTO `acos` VALUES ('111', '82', null, null, 'admin_dashboard', '69', '70');
INSERT INTO `acos` VALUES ('113', '1', null, null, 'Default', '100', '103');
INSERT INTO `acos` VALUES ('114', '113', null, null, 'oauth_2_callback', '101', '102');
INSERT INTO `acos` VALUES ('115', '1', null, null, 'SystemEvents', '104', '115');
INSERT INTO `acos` VALUES ('116', '115', null, null, 'admin_index', '105', '106');
INSERT INTO `acos` VALUES ('117', '115', null, null, 'admin_view', '107', '108');
INSERT INTO `acos` VALUES ('118', '115', null, null, 'admin_add', '109', '110');
INSERT INTO `acos` VALUES ('119', '115', null, null, 'admin_edit', '111', '112');
INSERT INTO `acos` VALUES ('120', '115', null, null, 'admin_delete', '113', '114');
INSERT INTO `acos` VALUES ('121', '1', null, null, 'SystemLogs', '116', '123');
INSERT INTO `acos` VALUES ('122', '121', null, null, 'admin_index', '117', '118');
INSERT INTO `acos` VALUES ('123', '121', null, null, 'admin_view', '119', '120');
INSERT INTO `acos` VALUES ('124', '121', null, null, 'admin_delete', '121', '122');
INSERT INTO `acos` VALUES ('125', '1', null, null, 'SystemMilestones', '124', '135');
INSERT INTO `acos` VALUES ('126', '125', null, null, 'admin_index', '125', '126');
INSERT INTO `acos` VALUES ('127', '125', null, null, 'admin_view', '127', '128');
INSERT INTO `acos` VALUES ('128', '125', null, null, 'admin_add', '129', '130');
INSERT INTO `acos` VALUES ('129', '125', null, null, 'admin_edit', '131', '132');
INSERT INTO `acos` VALUES ('130', '125', null, null, 'admin_delete', '133', '134');
INSERT INTO `acos` VALUES ('131', '1', null, null, 'SystemModelLogs', '136', '137');
INSERT INTO `acos` VALUES ('132', '1', null, null, 'SystemModels', '138', '149');
INSERT INTO `acos` VALUES ('133', '132', null, null, 'admin_index', '139', '140');
INSERT INTO `acos` VALUES ('134', '132', null, null, 'admin_view', '141', '142');
INSERT INTO `acos` VALUES ('135', '132', null, null, 'admin_add', '143', '144');
INSERT INTO `acos` VALUES ('136', '132', null, null, 'admin_edit', '145', '146');
INSERT INTO `acos` VALUES ('137', '132', null, null, 'admin_delete', '147', '148');
INSERT INTO `acos` VALUES ('138', '1', null, null, 'SystemStatuses', '150', '161');
INSERT INTO `acos` VALUES ('139', '138', null, null, 'admin_index', '151', '152');
INSERT INTO `acos` VALUES ('140', '138', null, null, 'admin_view', '153', '154');
INSERT INTO `acos` VALUES ('141', '138', null, null, 'admin_add', '155', '156');
INSERT INTO `acos` VALUES ('142', '138', null, null, 'admin_edit', '157', '158');
INSERT INTO `acos` VALUES ('143', '138', null, null, 'admin_delete', '159', '160');
INSERT INTO `acos` VALUES ('144', '1', null, null, 'SystemTasks', '162', '173');
INSERT INTO `acos` VALUES ('145', '144', null, null, 'index', '163', '164');
INSERT INTO `acos` VALUES ('146', '144', null, null, 'view', '165', '166');
INSERT INTO `acos` VALUES ('147', '144', null, null, 'add', '167', '168');
INSERT INTO `acos` VALUES ('148', '144', null, null, 'edit', '169', '170');
INSERT INTO `acos` VALUES ('149', '144', null, null, 'delete', '171', '172');
INSERT INTO `acos` VALUES ('150', '1', null, null, 'SystemTickets', '174', '185');
INSERT INTO `acos` VALUES ('151', '150', null, null, 'index', '175', '176');
INSERT INTO `acos` VALUES ('152', '150', null, null, 'view', '177', '178');
INSERT INTO `acos` VALUES ('153', '150', null, null, 'add', '179', '180');
INSERT INTO `acos` VALUES ('154', '150', null, null, 'edit', '181', '182');
INSERT INTO `acos` VALUES ('155', '150', null, null, 'delete', '183', '184');
INSERT INTO `acos` VALUES ('156', '1', null, null, 'Template', '186', '197');
INSERT INTO `acos` VALUES ('157', '156', null, null, 'admin_index', '187', '188');
INSERT INTO `acos` VALUES ('158', '156', null, null, 'admin_view', '189', '190');
INSERT INTO `acos` VALUES ('159', '156', null, null, 'admin_add', '191', '192');
INSERT INTO `acos` VALUES ('160', '156', null, null, 'admin_edit', '193', '194');
INSERT INTO `acos` VALUES ('161', '156', null, null, 'admin_delete', '195', '196');
INSERT INTO `acos` VALUES ('162', '82', null, null, 'mylvg_dashboard', '71', '72');
INSERT INTO `acos` VALUES ('163', '82', null, null, 'admin_delete_attachment', '73', '74');
INSERT INTO `acos` VALUES ('164', '82', null, null, 'admin_edit_user_prices', '75', '76');

-- ----------------------------
-- Table structure for `aros`
-- ----------------------------
DROP TABLE IF EXISTS `aros`;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` char(36) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` char(36) DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`) USING BTREE,
  KEY `idx_aros_alias` (`alias`) USING BTREE,
  KEY `idx_aros_model_foreign_key` (`model`,`foreign_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aros
-- ----------------------------
INSERT INTO `aros` VALUES ('1', null, 'Group', '51488314-33c4-4394-8d02-0f0c46dad844', '', '1', '2');
INSERT INTO `aros` VALUES ('2', null, 'Group', '5148e26b-f010-496e-b1c1-0f0c46dad844', '', '3', '4');
INSERT INTO `aros` VALUES ('3', null, 'Group', '5148e406-a598-4155-98b9-0f0c46dad844', '', '5', '6');
INSERT INTO `aros` VALUES ('4', null, 'Group', '5148e57b-aadc-4040-8633-0f0c46dad844', '', '7', '8');
INSERT INTO `aros` VALUES ('5', null, 'Group', '5148e587-2720-4203-a3d0-0f0c46dad844', '', '9', '10');

-- ----------------------------
-- Table structure for `aros_acos`
-- ----------------------------
DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_aros_acos_aro_id_aco_id` (`aro_id`,`aco_id`) USING BTREE,
  KEY `aco_id` (`aco_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aros_acos
-- ----------------------------
INSERT INTO `aros_acos` VALUES ('2', '1', '1', '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES ('3', '2', '1', '-1', '-1', '-1', '-1');
INSERT INTO `aros_acos` VALUES ('8', '3', '1', '-1', '-1', '-1', '-1');
INSERT INTO `aros_acos` VALUES ('9', '4', '1', '-1', '-1', '-1', '-1');
INSERT INTO `aros_acos` VALUES ('10', '5', '1', '-1', '-1', '-1', '-1');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` char(36) CHARACTER SET ascii NOT NULL,
  `system_status_id` char(36) DEFAULT '0',
  `name` varchar(140) CHARACTER SET ascii DEFAULT NULL,
  `description` text CHARACTER SET ascii,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('51488314-33c4-4394-8d02-0f0c46dad844', '0', 'System Administrator', 'System Administrators', '2013-03-19 15:24:04', '2013-03-19 15:24:04');
INSERT INTO `groups` VALUES ('5148e587-2720-4203-a3d0-0f0c46dad844', '0', 'Customer', 'Customers', '2013-03-19 22:24:07', '2013-03-19 22:24:07');

-- ----------------------------
-- Table structure for `key_value_pairs`
-- ----------------------------
DROP TABLE IF EXISTS `key_value_pairs`;
CREATE TABLE `key_value_pairs` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `key` varchar(120) DEFAULT NULL,
  `value` varchar(120) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of key_value_pairs
-- ----------------------------

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` char(36) NOT NULL DEFAULT '',
  `parent_page_id` char(36) DEFAULT NULL,
  `website_id` char(36) DEFAULT NULL,
  `system_status_id` char(36) DEFAULT '0',
  `meta_keywords` varchar(100) DEFAULT NULL,
  `meta_description` varchar(156) DEFAULT NULL,
  `dpr` varchar(50) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `content` text,
  `tags` varchar(255) DEFAULT NULL,
  `children` tinyint(1) DEFAULT '0',
  `position` int(2) DEFAULT NULL,
  `seo_priority` decimal(3,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages
-- ----------------------------

-- ----------------------------
-- Table structure for `system_events`
-- ----------------------------
DROP TABLE IF EXISTS `system_events`;
CREATE TABLE `system_events` (
  `id` char(36) NOT NULL,
  `system_model_id` char(36) DEFAULT NULL,
  `system_status_id` char(36) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `description` text,
  `url` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_events
-- ----------------------------
INSERT INTO `system_events` VALUES ('513e4dfa-9bd8-4878-b893-152c9699e514', '513e4c91-0064-4159-af1d-152c9699e514', '10', 'New Ticket', 'A new ticket #%s has been created by %s.', '/site-management/tickets/%s', '2013-03-11 21:34:50', '2013-03-11 21:34:50');

-- ----------------------------
-- Table structure for `system_logs`
-- ----------------------------
DROP TABLE IF EXISTS `system_logs`;
CREATE TABLE `system_logs` (
  `id` char(36) NOT NULL,
  `system_event_id` char(36) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `error` tinyint(1) DEFAULT '0',
  `summary` text,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_logs
-- ----------------------------

-- ----------------------------
-- Table structure for `system_milestones`
-- ----------------------------
DROP TABLE IF EXISTS `system_milestones`;
CREATE TABLE `system_milestones` (
  `id` char(36) NOT NULL DEFAULT '',
  `ref` int(8) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `due_date` datetime DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_milestones
-- ----------------------------
INSERT INTO `system_milestones` VALUES ('513e4269-c0b0-44ac-ac9e-152c9699e514', null, 'Brightsteel 1.0', 'The first release of aromanticgesture.com using the CakePHP Framework. Using: CakePHP 2.3.', '2013-04-01 11:00:00', null, '2013-03-11 20:45:29', '2013-03-11 20:45:29');

-- ----------------------------
-- Table structure for `system_models`
-- ----------------------------
DROP TABLE IF EXISTS `system_models`;
CREATE TABLE `system_models` (
  `id` char(36) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_models
-- ----------------------------
INSERT INTO `system_models` VALUES ('5182bf10-0568-48bc-a4da-0dc446dad844', 'Group', 'Group Model', '2013-05-02 19:31:28', '2013-05-02 19:31:28');
INSERT INTO `system_models` VALUES ('5182bf88-b3e0-43fe-b75f-0dc446dad844', 'User', 'User Model', '2013-05-02 19:33:28', '2013-05-02 19:33:28');
INSERT INTO `system_models` VALUES ('5182bfb2-a504-4a61-835a-0dc446dad844', 'Account', 'Account Model', '2013-05-02 19:34:10', '2013-05-02 19:34:10');

-- ----------------------------
-- Table structure for `system_model_logs`
-- ----------------------------
DROP TABLE IF EXISTS `system_model_logs`;
CREATE TABLE `system_model_logs` (
  `id` char(36) NOT NULL DEFAULT '',
  `system_model_id` char(36) DEFAULT NULL,
  `system_log_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_model_logs
-- ----------------------------

-- ----------------------------
-- Table structure for `system_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `system_statuses`;
CREATE TABLE `system_statuses` (
  `id` int(6) NOT NULL DEFAULT '0',
  `system_model_id` char(36) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_statuses
-- ----------------------------
INSERT INTO `system_statuses` VALUES ('1', null, 'In Development', 'This Ticket is currently being worked on.', '2013-03-09 21:13:28', '2013-03-09 21:38:59');
INSERT INTO `system_statuses` VALUES ('2', null, 'Live', 'This Ticket has been released.', '2013-03-09 21:24:19', '2013-03-09 21:24:19');
INSERT INTO `system_statuses` VALUES ('4', null, 'Duplicate', 'This Ticket is a duplicate of a previous ticket.', '2013-03-09 21:28:13', '2013-03-09 21:28:13');
INSERT INTO `system_statuses` VALUES ('5', null, 'Wont Fix', 'This Ticket will not be developed.', '2013-03-09 22:12:45', '2013-03-09 22:12:45');
INSERT INTO `system_statuses` VALUES ('3', null, 'Testing', 'This Ticket is currently being tested.', '2013-03-09 21:25:52', '2013-03-09 21:25:52');
INSERT INTO `system_statuses` VALUES ('6', null, 'Pending', 'This ticket is currently pending and will be looked at shortly.', '2013-03-11 19:33:23', '2013-03-11 19:33:23');
INSERT INTO `system_statuses` VALUES ('7', null, 'Information', 'New Information', '2013-03-11 21:32:10', '2013-03-11 21:32:10');
INSERT INTO `system_statuses` VALUES ('8', null, 'Warning', 'Be warned there is something afoot', '2013-03-11 21:32:31', '2013-03-11 21:32:31');
INSERT INTO `system_statuses` VALUES ('9', null, 'Errror', 'An Error has happened, do something', '2013-03-11 21:32:57', '2013-03-11 21:32:57');
INSERT INTO `system_statuses` VALUES ('10', null, 'Success', 'We are good to Go', '2013-03-11 21:33:26', '2013-03-11 21:33:26');

-- ----------------------------
-- Table structure for `system_tasks`
-- ----------------------------
DROP TABLE IF EXISTS `system_tasks`;
CREATE TABLE `system_tasks` (
  `id` char(36) DEFAULT NULL,
  `system_ticket_id` char(36) DEFAULT NULL,
  `assigned_id` char(36) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_tasks
-- ----------------------------

-- ----------------------------
-- Table structure for `system_tickets`
-- ----------------------------
DROP TABLE IF EXISTS `system_tickets`;
CREATE TABLE `system_tickets` (
  `id` char(36) NOT NULL DEFAULT '',
  `system_milestone_id` char(36) DEFAULT NULL,
  `reporter_id` char(36) DEFAULT NULL,
  `system_status_id` char(36) DEFAULT NULL,
  `ref` int(6) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_tickets
-- ----------------------------
INSERT INTO `system_tickets` VALUES ('513a672d-5ce4-42c6-acc9-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '1', '', 'ddd', '', '2013-03-08 22:33:17', '2013-03-11 19:38:03');
INSERT INTO `system_tickets` VALUES ('513a6763-8330-44ae-afe8-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '2', '', 'A', '', '2013-03-08 22:34:11', '2013-03-08 22:34:11');
INSERT INTO `system_tickets` VALUES ('513a676c-d330-4269-9f78-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '3', '', 'B', '', '2013-03-08 22:34:20', '2013-03-08 22:34:20');
INSERT INTO `system_tickets` VALUES ('513a6770-a2cc-429b-85ff-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '4', '', 'C', '', '2013-03-08 22:34:24', '2013-03-08 22:34:24');
INSERT INTO `system_tickets` VALUES ('513e42e6-8dc4-4e1d-87b2-152c9699e514', '513e4269-c0b0-44ac-ac9e-152c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '6', '5', '', 'System Logging', 'Log all types of events throughout the system.', '2013-03-11 20:47:34', '2013-03-11 20:47:34');
INSERT INTO `system_tickets` VALUES ('513e4349-a074-475a-9a34-152c9699e514', '513e4269-c0b0-44ac-ac9e-152c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '6', '6', '', 'Events and Event Types', 'Build an Event Manager.', '2013-03-11 20:49:13', '2013-03-11 20:49:13');
INSERT INTO `system_tickets` VALUES ('513e4394-b810-464a-87ff-152c9699e514', '513e4269-c0b0-44ac-ac9e-152c9699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '6', '7', '', 'Build CMS', 'Pages,\r\nSections,\r\n', '2013-03-11 20:50:28', '2013-03-11 20:50:28');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) CHARACTER SET ascii NOT NULL,
  `group_id` char(36) CHARACTER SET ascii DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `system_status_id` int(6) DEFAULT NULL,
  `email` varchar(200) CHARACTER SET ascii DEFAULT NULL,
  `password` varchar(120) CHARACTER SET ascii DEFAULT NULL,
  `title` varchar(8) CHARACTER SET ascii DEFAULT NULL,
  `firstname` varchar(120) CHARACTER SET ascii DEFAULT NULL,
  `lastname` varchar(120) CHARACTER SET ascii DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `phone` varchar(13) CHARACTER SET ascii DEFAULT NULL,
  `mobile` varchar(13) CHARACTER SET ascii DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('5148841b-9028-4ee0-878e-0f0c46dad844', '51488314-33c4-4394-8d02-0f0c46dad844', null, null, 'sidmalde@gmail.com', 'd9accd5d47764e58eeee6f8f4efcbddc', 'Mr', 'Sid', 'Malde', '1993-01-02', 'Male', '447545976559', '447545976559', null, null, '2013-03-19 15:28:27', '2013-03-19 15:28:27');

-- ----------------------------
-- Table structure for `websites`
-- ----------------------------
DROP TABLE IF EXISTS `websites`;
CREATE TABLE `websites` (
  `id` char(36) NOT NULL DEFAULT '',
  `system_status_id` char(36) DEFAULT NULL,
  `currency` char(5) DEFAULT 'EUR',
  `display_rrp` tinyint(1) DEFAULT '1',
  `currency_iso` varchar(3) DEFAULT NULL,
  `country_iso2` varchar(2) DEFAULT NULL,
  `country_iso3` varchar(3) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `language` char(3) DEFAULT NULL,
  `language_short` char(2) DEFAULT NULL,
  `google_analytics_code` varchar(20) DEFAULT NULL,
  `adwords_id` varchar(10) DEFAULT NULL,
  `adwords_language` varchar(5) DEFAULT NULL,
  `adwords_label` varchar(20) DEFAULT NULL,
  `adwords_value` decimal(10,2) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `tagline` varchar(60) DEFAULT NULL,
  `company` varchar(40) DEFAULT NULL,
  `address_1` varchar(40) DEFAULT NULL,
  `address_2` varchar(40) DEFAULT NULL,
  `city` varchar(40) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `legal` text,
  `support_email` varchar(50) DEFAULT NULL,
  `sales_email` varchar(50) DEFAULT NULL,
  `invoice_email` varchar(50) DEFAULT NULL,
  `keywords` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of websites
-- ----------------------------
