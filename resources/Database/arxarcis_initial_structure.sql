/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : sidmalde_corbomite

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-03-21 20:33:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acos`
-- ----------------------------
DROP TABLE IF EXISTS `acos`;
CREATE TABLE `acos` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`parent_id`  int(11) NULL DEFAULT NULL ,
`model`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
`foreign_key`  int(10) UNSIGNED NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
`lft`  int(11) NULL DEFAULT NULL ,
`rght`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `idx_acos_lft_rght` (`lft`, `rght`) USING BTREE ,
INDEX `idx_acos_alias` (`alias`) USING BTREE ,
INDEX `idx_acos_model_foreign_key` (`model`, `foreign_key`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=113

;

-- ----------------------------
-- Records of acos
-- ----------------------------
BEGIN;
INSERT INTO `acos` VALUES ('1', null, null, null, 'controllers', '1', '204'), ('2', '1', null, null, 'Acl', '2', '17'), ('3', '2', null, null, 'startup', '3', '4'), ('4', '2', null, null, 'aco_aro_sync', '5', '6'), ('5', '2', null, null, 'aco_update', '7', '8'), ('6', '2', null, null, 'getControllerList', '9', '10'), ('7', '2', null, null, 'verify', '11', '12'), ('8', '2', null, null, 'recover', '13', '14'), ('9', '2', null, null, 'init_group_permissions', '15', '16'), ('10', '1', null, null, 'EventTypes', '18', '29'), ('11', '10', null, null, 'index', '19', '20'), ('12', '10', null, null, 'view', '21', '22'), ('13', '10', null, null, 'add', '23', '24'), ('14', '10', null, null, 'edit', '25', '26'), ('15', '10', null, null, 'delete', '27', '28'), ('16', '1', null, null, 'Events', '30', '41'), ('17', '16', null, null, 'index', '31', '32'), ('18', '16', null, null, 'view', '33', '34'), ('19', '16', null, null, 'add', '35', '36'), ('20', '16', null, null, 'edit', '37', '38'), ('21', '16', null, null, 'delete', '39', '40'), ('22', '1', null, null, 'Groups', '42', '53'), ('28', '1', null, null, 'Logs', '54', '65'), ('29', '28', null, null, 'index', '55', '56'), ('30', '28', null, null, 'view', '57', '58'), ('31', '28', null, null, 'add', '59', '60'), ('32', '28', null, null, 'edit', '61', '62'), ('33', '28', null, null, 'delete', '63', '64'), ('34', '1', null, null, 'Milestones', '66', '77'), ('35', '34', null, null, 'index', '67', '68'), ('36', '34', null, null, 'view', '69', '70'), ('37', '34', null, null, 'add', '71', '72'), ('38', '34', null, null, 'edit', '73', '74'), ('39', '34', null, null, 'delete', '75', '76'), ('40', '1', null, null, 'ModelLogs', '78', '89'), ('41', '40', null, null, 'index', '79', '80'), ('42', '40', null, null, 'view', '81', '82'), ('43', '40', null, null, 'add', '83', '84'), ('44', '40', null, null, 'edit', '85', '86'), ('45', '40', null, null, 'delete', '87', '88'), ('46', '1', null, null, 'PageHistories', '90', '101'), ('47', '46', null, null, 'index', '91', '92'), ('48', '46', null, null, 'view', '93', '94'), ('49', '46', null, null, 'add', '95', '96'), ('50', '46', null, null, 'edit', '97', '98'), ('51', '46', null, null, 'delete', '99', '100'), ('52', '1', null, null, 'Pages', '102', '113'), ('53', '52', null, null, 'index', '103', '104'), ('54', '52', null, null, 'view', '105', '106'), ('55', '52', null, null, 'add', '107', '108'), ('56', '52', null, null, 'edit', '109', '110'), ('57', '52', null, null, 'delete', '111', '112'), ('58', '1', null, null, 'StatusTypes', '114', '125'), ('59', '58', null, null, 'index', '115', '116'), ('60', '58', null, null, 'view', '117', '118'), ('61', '58', null, null, 'add', '119', '120'), ('62', '58', null, null, 'edit', '121', '122'), ('63', '58', null, null, 'delete', '123', '124'), ('64', '1', null, null, 'Statuses', '126', '137'), ('65', '64', null, null, 'index', '127', '128'), ('66', '64', null, null, 'view', '129', '130'), ('67', '64', null, null, 'add', '131', '132'), ('68', '64', null, null, 'edit', '133', '134'), ('69', '64', null, null, 'delete', '135', '136'), ('70', '1', null, null, 'Tasks', '138', '149'), ('71', '70', null, null, 'index', '139', '140'), ('72', '70', null, null, 'view', '141', '142'), ('73', '70', null, null, 'add', '143', '144'), ('74', '70', null, null, 'edit', '145', '146'), ('75', '70', null, null, 'delete', '147', '148'), ('76', '1', null, null, 'Tickets', '150', '161'), ('77', '76', null, null, 'index', '151', '152'), ('78', '76', null, null, 'view', '153', '154'), ('79', '76', null, null, 'add', '155', '156'), ('80', '76', null, null, 'edit', '157', '158'), ('81', '76', null, null, 'delete', '159', '160'), ('82', '1', null, null, 'Users', '162', '181'), ('88', '82', null, null, 'login', '163', '164'), ('89', '82', null, null, 'logout', '165', '166'), ('90', '1', null, null, 'Websites', '182', '193'), ('91', '90', null, null, 'index', '183', '184'), ('92', '90', null, null, 'view', '185', '186'), ('93', '90', null, null, 'add', '187', '188'), ('94', '90', null, null, 'edit', '189', '190'), ('95', '90', null, null, 'delete', '191', '192'), ('96', '1', null, null, 'AclExtras', '194', '195'), ('97', '1', null, null, 'DebugKit', '196', '203'), ('98', '97', null, null, 'ToolbarAccess', '197', '202'), ('99', '98', null, null, 'history_state', '198', '199'), ('100', '98', null, null, 'sql_explain', '200', '201'), ('101', '22', null, null, 'admin_index', '43', '44'), ('102', '22', null, null, 'admin_view', '45', '46'), ('103', '22', null, null, 'admin_add', '47', '48'), ('104', '22', null, null, 'admin_edit', '49', '50'), ('105', '22', null, null, 'admin_delete', '51', '52'), ('106', '82', null, null, 'admin_index', '167', '168'), ('107', '82', null, null, 'admin_view', '169', '170'), ('108', '82', null, null, 'admin_add', '171', '172'), ('109', '82', null, null, 'admin_edit', '173', '174'), ('110', '82', null, null, 'admin_delete', '175', '176');
INSERT INTO `acos` VALUES ('111', '82', null, null, 'admin_dashboard', '177', '178'), ('112', '82', null, null, 'manage_dashboard', '179', '180');
COMMIT;

-- ----------------------------
-- Table structure for `aros`
-- ----------------------------
DROP TABLE IF EXISTS `aros`;
CREATE TABLE `aros` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`parent_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`model`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
`foreign_key`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
`lft`  int(11) NULL DEFAULT NULL ,
`rght`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `idx_aros_lft_rght` (`lft`, `rght`) USING BTREE ,
INDEX `idx_aros_alias` (`alias`) USING BTREE ,
INDEX `idx_aros_model_foreign_key` (`model`, `foreign_key`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Records of aros
-- ----------------------------
BEGIN;
INSERT INTO `aros` VALUES ('1', null, 'Group', '51488314-33c4-4394-8d02-0f0c46dad844', '', '1', '2'), ('2', null, 'Group', '5148e26b-f010-496e-b1c1-0f0c46dad844', '', '3', '4'), ('3', null, 'Group', '5148e406-a598-4155-98b9-0f0c46dad844', '', '5', '6'), ('4', null, 'Group', '5148e57b-aadc-4040-8633-0f0c46dad844', '', '7', '8'), ('5', null, 'Group', '5148e587-2720-4203-a3d0-0f0c46dad844', '', '9', '10');
COMMIT;

-- ----------------------------
-- Table structure for `aros_acos`
-- ----------------------------
DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE `aros_acos` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`aro_id`  int(10) NOT NULL ,
`aco_id`  int(10) UNSIGNED NOT NULL ,
`_create`  char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' ,
`_read`  char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' ,
`_update`  char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' ,
`_delete`  char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `idx_aros_acos_aro_id_aco_id` (`aro_id`, `aco_id`) USING BTREE ,
INDEX `aco_id` (`aco_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=11

;

-- ----------------------------
-- Records of aros_acos
-- ----------------------------
BEGIN;
INSERT INTO `aros_acos` VALUES ('2', '1', '1', '1', '1', '1', '1'), ('3', '2', '1', '-1', '-1', '-1', '-1'), ('4', '2', '112', '1', '1', '1', '1'), ('5', '2', '34', '1', '1', '1', '1'), ('6', '2', '76', '1', '1', '1', '1'), ('7', '2', '70', '1', '1', '1', '1'), ('8', '3', '1', '-1', '-1', '-1', '-1'), ('9', '4', '1', '-1', '-1', '-1', '-1'), ('10', '5', '1', '-1', '-1', '-1', '-1');
COMMIT;

-- ----------------------------
-- Table structure for `event_types`
-- ----------------------------
DROP TABLE IF EXISTS `event_types`;
CREATE TABLE `event_types` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`name`  varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of event_types
-- ----------------------------
BEGIN;
INSERT INTO `event_types` VALUES ('513e4c1a-7520-41a1-82fb-152c9699e514', 'System', 'System Related Events', '2013-03-11 21:26:50', '2013-03-11 21:26:50'), ('513e4c42-c8fc-4bb9-88a7-152c9699e514', 'Pages', 'Events related to Pages', '2013-03-11 21:27:30', '2013-03-11 21:27:30'), ('513e4c5c-5b70-4284-8f8e-152c9699e514', 'Users', 'Events related to Users', '2013-03-11 21:27:56', '2013-03-11 21:27:56'), ('513e4c6b-3f30-4ca7-86f7-152c9699e514', 'Groups', 'Events related to Groups', '2013-03-11 21:28:11', '2013-03-11 21:28:11'), ('513e4c91-0064-4159-af1d-152c9699e514', 'Tickets', 'Events related to Tickets', '2013-03-11 21:28:49', '2013-03-11 21:28:49'), ('51424778-a9a8-4112-92c1-1b0c46dad844', '', '', '2013-03-14 21:56:08', '2013-03-14 21:56:08');
COMMIT;

-- ----------------------------
-- Table structure for `events`
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`event_type_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`name`  varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`url`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of events
-- ----------------------------
BEGIN;
INSERT INTO `events` VALUES ('513e4dfa-9bd8-4878-b893-152c9699e514', '513e4c91-0064-4159-af1d-152c9699e514', '10', 'New Ticket', 'A new ticket #%s has been created by %s.', '/site-management/tickets/%s', '2013-03-11 21:34:50', '2013-03-11 21:34:50');
COMMIT;

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
`id`  char(36) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL ,
`name`  varchar(140) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET ascii COLLATE ascii_general_ci NULL ,
`deleted`  tinyint(1) NULL DEFAULT 0 ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of groups
-- ----------------------------
BEGIN;
INSERT INTO `groups` VALUES ('5148e26b-f010-496e-b1c1-0f0c46dad844', 'Company Director', 'Company Directors', '0', '2013-03-19 22:10:51', '2013-03-19 22:10:51'), ('51488314-33c4-4394-8d02-0f0c46dad844', 'System Administrator', 'System Administrators', '0', '2013-03-19 15:24:04', '2013-03-19 15:24:04'), ('5148e406-a598-4155-98b9-0f0c46dad844', 'Driver', 'Drivers', '0', '2013-03-19 22:17:42', '2013-03-19 22:17:42'), ('5148e57b-aadc-4040-8633-0f0c46dad844', 'Agent', 'Agents', '0', '2013-03-19 22:23:55', '2013-03-19 22:23:55'), ('5148e587-2720-4203-a3d0-0f0c46dad844', 'Client', 'Clients', '0', '2013-03-19 22:24:07', '2013-03-19 22:24:07');
COMMIT;

-- ----------------------------
-- Table structure for `logs`
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`event_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`admin`  tinyint(1) NOT NULL DEFAULT 0 ,
`error`  tinyint(1) NULL DEFAULT 0 ,
`summary`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`created`  datetime NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `milestones`
-- ----------------------------
DROP TABLE IF EXISTS `milestones`;
CREATE TABLE `milestones` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`ref`  int(8) NULL DEFAULT NULL ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`due_date`  datetime NULL DEFAULT NULL ,
`completed_date`  datetime NULL DEFAULT NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of milestones
-- ----------------------------
BEGIN;
INSERT INTO `milestones` VALUES ('513e4269-c0b0-44ac-ac9e-152c9699e514', null, 'Brightsteel 1.0', 'The first release of aromanticgesture.com using the CakePHP Framework. Using: CakePHP 2.3.', '2013-04-01 11:00:00', null, '2013-03-11 20:45:29', '2013-03-11 20:45:29');
COMMIT;

-- ----------------------------
-- Table structure for `model_logs`
-- ----------------------------
DROP TABLE IF EXISTS `model_logs`;
CREATE TABLE `model_logs` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`model_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`log_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of model_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `models`
-- ----------------------------
DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of models
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `page_histories`
-- ----------------------------
DROP TABLE IF EXISTS `page_histories`;
CREATE TABLE `page_histories` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`page_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`parent_page_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`website_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`page_layout_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`dpr`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`label`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`publish`  tinyint(1) NULL DEFAULT 0 ,
`moveable`  tinyint(1) NULL DEFAULT 0 ,
`edit_page_layout`  tinyint(1) NULL DEFAULT 0 ,
`edit_dpr`  tinyint(1) NULL DEFAULT 0 ,
`edit_url`  tinyint(1) NULL DEFAULT 0 ,
`edit_content`  tinyint(1) NULL DEFAULT 0 ,
`deletable`  tinyint(1) NULL DEFAULT 0 ,
`children`  tinyint(1) NULL DEFAULT 0 ,
`position`  int(2) NULL DEFAULT NULL ,
`expires`  datetime NULL DEFAULT NULL ,
`seo_priority`  decimal(3,2) NULL DEFAULT NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of page_histories
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`parent_page_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`website_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`meta_keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`meta_description`  varchar(156) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`dpr`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`label`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`tags`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`children`  tinyint(1) NULL DEFAULT 0 ,
`position`  int(2) NULL DEFAULT NULL ,
`seo_priority`  decimal(3,2) NULL DEFAULT NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of pages
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `status_types`
-- ----------------------------
DROP TABLE IF EXISTS `status_types`;
CREATE TABLE `status_types` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`name`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of status_types
-- ----------------------------
BEGIN;
INSERT INTO `status_types` VALUES ('513b87db-23cc-4b73-8993-07449699e514', 'System', 'For System Level Statuses', '2013-03-09 19:04:59', '2013-03-09 20:56:26'), ('513b88ae-e788-46dc-bb94-07449699e514', 'Ticket', 'For Ticket Related Statuses', '2013-03-09 19:08:30', '2013-03-09 19:08:30');
COMMIT;

-- ----------------------------
-- Table structure for `statuses`
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
`id`  int(6) NOT NULL DEFAULT 0 ,
`status_type_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of statuses
-- ----------------------------
BEGIN;
INSERT INTO `statuses` VALUES ('1', '513b88ae-e788-46dc-bb94-07449699e514', 'In Development', 'This Ticket is currently being worked on.', '2013-03-09 21:13:28', '2013-03-09 21:38:59'), ('2', '513b88ae-e788-46dc-bb94-07449699e514', 'Live', 'This Ticket has been released.', '2013-03-09 21:24:19', '2013-03-09 21:24:19'), ('4', '513b88ae-e788-46dc-bb94-07449699e514', 'Duplicate', 'This Ticket is a duplicate of a previous ticket.', '2013-03-09 21:28:13', '2013-03-09 21:28:13'), ('5', '513b88ae-e788-46dc-bb94-07449699e514', 'Wont Fix', 'This Ticket will not be developed.', '2013-03-09 22:12:45', '2013-03-09 22:12:45'), ('3', '513b88ae-e788-46dc-bb94-07449699e514', 'Testing', 'This Ticket is currently being tested.', '2013-03-09 21:25:52', '2013-03-09 21:25:52'), ('6', '513b88ae-e788-46dc-bb94-07449699e514', 'Pending', 'This ticket is currently pending and will be looked at shortly.', '2013-03-11 19:33:23', '2013-03-11 19:33:23'), ('7', '513b87db-23cc-4b73-8993-07449699e514', 'Information', 'New Information', '2013-03-11 21:32:10', '2013-03-11 21:32:10'), ('8', '513b87db-23cc-4b73-8993-07449699e514', 'Warning', 'Be warned there is something afoot', '2013-03-11 21:32:31', '2013-03-11 21:32:31'), ('9', '513b87db-23cc-4b73-8993-07449699e514', 'Errror', 'An Error has happened, do something', '2013-03-11 21:32:57', '2013-03-11 21:32:57'), ('10', '513b87db-23cc-4b73-8993-07449699e514', 'Success', 'We are good to Go', '2013-03-11 21:33:26', '2013-03-11 21:33:26');
COMMIT;

-- ----------------------------
-- Table structure for `tasks`
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`ticket_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`assigned_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`summary`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of tasks
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `tickets`
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`milestone_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`previous_milestone_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`reporter_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`manager_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`priority_id`  int(6) NULL DEFAULT NULL ,
`status_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`ref`  int(6) NULL DEFAULT NULL ,
`url`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`summary`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of tickets
-- ----------------------------
BEGIN;
INSERT INTO `tickets` VALUES ('513a672d-5ce4-42c6-acc9-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '1', '1', '', 'ddd', '', '2013-03-08 22:33:17', '2013-03-11 19:38:03'), ('513a6763-8330-44ae-afe8-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '1', '2', '', 'A', '', '2013-03-08 22:34:11', '2013-03-08 22:34:11'), ('513a676c-d330-4269-9f78-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '1', '3', '', 'B', '', '2013-03-08 22:34:20', '2013-03-08 22:34:20'), ('513a6770-a2cc-429b-85ff-26c89699e514', '5137d9b0-c330-4b6b-9170-056c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '1', '1', '4', '', 'C', '', '2013-03-08 22:34:24', '2013-03-08 22:34:24'), ('513e42e6-8dc4-4e1d-87b2-152c9699e514', '513e4269-c0b0-44ac-ac9e-152c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '3', '6', '5', '', 'System Logging', 'Log all types of events throughout the system.', '2013-03-11 20:47:34', '2013-03-11 20:47:34'), ('513e4349-a074-475a-9a34-152c9699e514', '513e4269-c0b0-44ac-ac9e-152c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '3', '6', '6', '', 'Events and Event Types', 'Build an Event Manager.', '2013-03-11 20:49:13', '2013-03-11 20:49:13'), ('513e4394-b810-464a-87ff-152c9699e514', '513e4269-c0b0-44ac-ac9e-152c9699e514', null, '512d5470-f6ec-4689-ae78-0ed89699e514', '512d5470-f6ec-4689-ae78-0ed89699e514', '3', '6', '7', '', 'Build CMS', 'Pages,\r\nSections,\r\n', '2013-03-11 20:50:28', '2013-03-11 20:50:28');
COMMIT;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
`id`  char(36) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL ,
`group_id`  char(36) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`status_id`  int(6) NULL DEFAULT NULL ,
`ref`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`email`  varchar(200) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`password`  varchar(120) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`title`  varchar(8) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`firstname`  varchar(120) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`lastname`  varchar(120) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`address_1`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`address_2`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`address_3`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`city`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`postcode`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`country`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`date_of_birth`  date NULL DEFAULT NULL ,
`gender`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`phone`  varchar(13) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`mobile`  varchar(13) CHARACTER SET ascii COLLATE ascii_general_ci NULL DEFAULT NULL ,
`ni_number`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`pco_license_no`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`driving_license_no`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`passport_no`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`date_comenced`  date NULL DEFAULT NULL ,
`date_terminated`  date NULL DEFAULT NULL ,
`pco_license`  tinyint(1) NULL DEFAULT NULL ,
`pco_counterpart`  tinyint(1) NULL DEFAULT NULL ,
`driving_license`  tinyint(1) NULL DEFAULT NULL ,
`driving_counterpart`  tinyint(1) NULL DEFAULT NULL ,
`passport`  tinyint(1) NULL DEFAULT NULL ,
`photo`  tinyint(1) NULL DEFAULT NULL ,
`confidentiality_agreement`  tinyint(1) NULL DEFAULT NULL ,
`notes`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`driver_signature`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`driver_name`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`position`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`signature`  tinyint(1) NULL DEFAULT NULL ,
`active`  tinyint(1) NULL DEFAULT 1 ,
`deleted`  tinyint(1) NULL DEFAULT 0 ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('5148841b-9028-4ee0-878e-0f0c46dad844', '51488314-33c4-4394-8d02-0f0c46dad844', null, null, 'sidmalde@gmail.com', 'd9accd5d47764e58eeee6f8f4efcbddc', 'Mr', 'Sid', 'Malde', null, null, null, null, null, null, '1993-01-02', 'Male', '447545976559', '447545976559', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '0', '2013-03-19 15:28:27', '2013-03-19 15:28:27'), ('514b47a1-4ce0-4c72-aa20-0f0c46dad844', '5148e26b-f010-496e-b1c1-0f0c46dad844', null, null, 'max@londonvipgroup.com', 'af4c0ab6de986158c3a3aa7390b81a3a', 'Mr', 'Max', 'Maksoud', null, null, null, null, null, null, '1993-03-21', 'Male', '02076252666', '07979044444', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '0', '2013-03-21 17:47:13', '2013-03-21 17:47:13'), ('514b47d9-d88c-42c1-95d7-0f0c46dad844', '5148e26b-f010-496e-b1c1-0f0c46dad844', null, null, 'sam@londonvipgroup.com', 'af4c0ab6de986158c3a3aa7390b81a3a', 'Mr', 'Sam', 'Aly', null, null, null, null, null, null, '1993-03-21', 'Male', '02076252666', '07855555266', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '0', '2013-03-21 17:48:09', '2013-03-21 17:48:09');
COMMIT;

-- ----------------------------
-- Table structure for `websites`
-- ----------------------------
DROP TABLE IF EXISTS `websites`;
CREATE TABLE `websites` (
`id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`status_id`  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`currency`  char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EUR' ,
`display_rrp`  tinyint(1) NULL DEFAULT 1 ,
`currency_iso`  varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`country_iso2`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`country_iso3`  varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`language`  char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`language_short`  char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`google_analytics_code`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`adwords_id`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`adwords_language`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`adwords_label`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`adwords_value`  decimal(10,2) NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`tagline`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`company`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`address_1`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`address_2`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`city`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`postcode`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`country`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`phone`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`fax`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`copyright`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`legal`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`support_email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sales_email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`invoice_email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created`  datetime NULL DEFAULT NULL ,
`modified`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `url` (`url`) USING BTREE 
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of websites
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Auto increment value for `acos`
-- ----------------------------
ALTER TABLE `acos` AUTO_INCREMENT=113;

-- ----------------------------
-- Auto increment value for `aros`
-- ----------------------------
ALTER TABLE `aros` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for `aros_acos`
-- ----------------------------
ALTER TABLE `aros_acos` AUTO_INCREMENT=11;
